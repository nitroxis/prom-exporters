use std::{
	collections::HashMap,
	fmt::Write,
	io::ErrorKind,
	sync::Arc,
	time::{Duration, Instant},
};

use axum::{
	extract::{Path, Request},
	routing::{get, post},
	Extension, Router,
};
use clap::Parser;
use colored::*;
use futures_util::TryStreamExt;
use regex::Regex;
use scopeguard::defer;
use tokio::{
	io::AsyncBufReadExt,
	sync::{Mutex, RwLock},
	time::sleep,
};
use tokio_util::io::StreamReader;
use tracing_subscriber::{fmt, prelude::*, EnvFilter};

const AVG_WINDOW: f64 = 2.0f64;

#[derive(Parser, Debug)]
struct Opts {
	/// Minimum FPS, all values below this will be printed red.
	#[arg(long)]
	min_fps: Option<f64>,

	/// Minimum speed, all values below this will be printed red.
	#[arg(long)]
	min_speed: Option<f64>,

	/// Print interval
	#[arg(short, long, default_value = "0.2")]
	interval: f64,

	/// HTTP listen end point.
	#[arg(short, long, default_value = "0.0.0.0:9191")]
	listen_addr: String,
}

#[derive(Clone, Eq, PartialEq, Hash)]
struct StreamField {
	file: i32,
	stream: i32,
	key: String,
}

#[derive(Clone, Default)]
struct Stats {
	frame: f64,
	time: f64,
	size: f64,
}

#[derive(Clone, Default)]
struct Progress {
	stats: Stats,
	streams: HashMap<StreamField, f64>,
}

struct Metrics {
	progress: RwLock<HashMap<String, Progress>>,
}

#[tokio::main]
async fn main() {
	tracing_subscriber::registry()
		.with(fmt::layer())
		.with(EnvFilter::from_default_env())
		.init();

	let opts = Opts::parse();
	let opts = Arc::new(opts);

	let m = Metrics {
		progress: RwLock::new(HashMap::new()),
	};

	let app = Router::new()
		.route("/progress/:name", post(progress))
		.route("/metrics", get(metrics))
		.layer(Extension(opts.clone()))
		.layer(Extension(Arc::new(m)));

	let listener = tokio::net::TcpListener::bind(&opts.listen_addr).await.unwrap();
	axum::serve(listener, app).await.unwrap();
}

async fn metrics(metrics: Extension<Arc<Metrics>>) -> String {
	let metrics = metrics.progress.read().await;

	let mut res = String::new();

	writeln!(res, "# TYPE ffmpeg_frames gauge").unwrap();
	writeln!(res, "# TYPE ffmpeg_size gauge").unwrap();
	writeln!(res, "# TYPE ffmpeg_time gauge").unwrap();
	writeln!(res, "# TYPE ffmpeg_stream gauge").unwrap();

	for (name, progress) in metrics.iter() {
		writeln!(res, "ffmpeg_frames{{name=\"{name}\"}} {}", progress.stats.frame).unwrap();
		writeln!(res, "ffmpeg_size{{name=\"{name}\"}} {}", progress.stats.size).unwrap();
		writeln!(res, "ffmpeg_time{{name=\"{name}\"}} {}", progress.stats.time).unwrap();
		for (k, v) in progress.streams.iter() {
			writeln!(
				res,
				"ffmpeg_stream{{name=\"{name}\", file=\"{}\", stream=\"{}\", key=\"{}\"}} {}",
				k.file, k.stream, k.key, v
			)
			.unwrap();
		}
	}

	res
}

async fn progress(metrics: Extension<Arc<Metrics>>, opts: Extension<Arc<Opts>>, Path(name): Path<String>, request: Request) {
	{
		let mut metrics = metrics.progress.write().await;
		if metrics.contains_key(&name) {
			tracing::error!(name, "Name is already in use");
			return;
		}

		metrics.insert(name.clone(), Progress::default());
	}

	let name2 = name.clone();
	let metrics2 = metrics.clone();
	defer! {
		tokio::spawn(async move {
			let mut metrics = metrics2.progress.write().await;
			metrics.remove(&name2);
		});
	}

	let body = request
		.into_body()
		.into_data_stream()
		.map_err(|e| std::io::Error::new(ErrorKind::Other, e));
	let reader = StreamReader::new(body);
	let mut lines = reader.lines();

	let mut current_streams = HashMap::new();
	let mut current_stats = Stats::default();
	let stats: Arc<Mutex<Vec<(Instant, Stats)>>> = Arc::new(Mutex::new(Vec::new()));

	let printer = async {
		let interval = Duration::from_secs_f64(opts.interval);

		loop {
			sleep(interval).await;

			let history = stats.lock().await;
			if history.len() < 1 {
				continue;
			}

			let (t1, v1) = history[history.len() - 1].clone();
			let t0 = t1 - Duration::from_secs_f64(AVG_WINDOW);
			let v0 = lerp(history.as_slice(), t0).1;

			let fps = (v1.frame - v0.frame) / AVG_WINDOW;
			let speed = (v1.time - v0.time) / AVG_WINDOW;
			let bitrate = (v1.size - v0.size) / AVG_WINDOW / (1024f64 / 8f64);

			let text = format!("FPS: {fps:>7.2}, Speed: {speed:>6.3}, Bitrate: {bitrate:>9.2} kbits/s");

			let mut is_warn = false;

			if let Some(min) = opts.min_fps {
				if fps < min {
					is_warn = true;
				}
			}

			if let Some(min) = opts.min_speed {
				if speed < min {
					is_warn = true;
				}
			}

			if is_warn {
				println!("{}", text.red());
			} else {
				println!("{}", text);
			}
		}
	};

	let reader = async {
		let stream_regex = Regex::new(r"^stream_(\d+)_(\d+)_(.+)$").unwrap();

		while let Ok(Some(line)) = lines.next_line().await {
			if let Some((k, v)) = line.split_once('=') {
				match k {
					"frame" => {
						let v: f64 = v.parse().unwrap_or_default();
						tracing::debug!(v, "frame");
						current_stats.frame = v;
					}
					"out_time_us" => {
						let v: f64 = v.parse().unwrap_or_default();
						tracing::debug!(v, "out_time_us");
						if v >= 0.0 {
							let v = v / 1_000_000.0f64;
							current_stats.time = v;
						}
					}
					"total_size" => {
						let v: f64 = v.parse().unwrap_or_default();
						tracing::debug!(v, "total_size");
						current_stats.size = v;
					}
					"progress" => {
						tracing::debug!("progress");

						// Update local stats history.
						{
							let mut lock = stats.lock().await;

							let cutoff = Instant::now() - Duration::from_secs_f64(AVG_WINDOW) - Duration::from_secs(1);

							while lock.len() > 0 && lock[0].0 < cutoff {
								lock.remove(0);
							}

							lock.push((Instant::now(), current_stats.clone()));
						}

						// Update global progress table.
						{
							let mut metrics = metrics.progress.write().await;
							let progress = metrics.get_mut(&name).unwrap();
							progress.stats = current_stats.clone();
							progress.streams = current_streams.clone();
						}
					}
					_ => {
						if let Some(cap) = stream_regex.captures(k) {
							let v: f64 = v.parse().unwrap_or_default();
							let file: i32 = cap[1].parse().unwrap_or_default();
							let stream: i32 = cap[2].parse().unwrap_or_default();
							let key = cap[3].to_string();

							tracing::debug!(file, stream, key, v, "stream");
							current_streams.insert(StreamField { file, stream, key }, v);
						}
					}
				}
			}
		}
	};

	tokio::select!(
		_ = printer => {},
		_ = reader => {}
	);
}

fn lerp(history: &[(Instant, Stats)], t: Instant) -> (usize, Stats) {
	match history.binary_search_by(|(t2, _)| t2.cmp(&t)) {
		Ok(i) => (i, history[i].1.clone()),
		Err(i) => {
			if i == 0 {
				(0, history[0].1.clone())
			} else if i == history.len() {
				(i - 1, history[history.len() - 1].1.clone())
			} else {
				let (t0, v0) = &history[i - 1];
				let (t1, v1) = &history[i];

				let dt = t.duration_since(*t0).as_secs_f64();
				let time_span = t1.duration_since(*t0).as_secs_f64();
				let f = dt / time_span;
				let lerped = Stats {
					frame: v0.frame + (v1.frame - v0.frame) * f,
					time: v0.time + (v1.time - v0.time) * f,
					size: v0.size + (v1.size - v0.size) * f,
				};
				(i - 1, lerped)
			}
		}
	}
}

use std::{
	sync::{
		atomic::{AtomicBool, Ordering},
		Arc,
	},
	thread,
	time::Duration,
};

use bme280::i2c::BME280;
use clap::Parser;
use linux_embedded_hal::{Delay, I2cdev};
use prometheus_exporter::prometheus::register_gauge;
use tracing_subscriber::{fmt, prelude::*, EnvFilter};

#[derive(Debug, Parser)]
struct Opts {
	/// The HTTP listen address
	#[clap(short, long, default_value = "0.0.0.0:9196")]
	listen_addr: String,

	/// The I²C device path (e.g. /dev/i2c-0).
	device: String,

	/// The I²C address.
	#[clap(short, long, default_value = "118")]
	addr: u8,
}

fn main() {
	tracing_subscriber::registry()
		.with(fmt::layer())
		.with(EnvFilter::from_default_env())
		.init();

	let opts = Opts::parse();

	prometheus_exporter::start(opts.listen_addr.parse().expect("parse listen addr"))
		.expect("failed to start prometheus exporter");

	let metric_temperature = register_gauge!("bme280_temperature", "BME280 temperature in celsius").expect("register metric");
	let metric_humidity = register_gauge!("bme280_humidity", "BME280 relative humidity in %").expect("register metric");
	let metric_pressure = register_gauge!("bme280_pressure", "BME280 pressure in pascal").expect("register metric");

	let i2c_bus = I2cdev::new(&opts.device).expect("open i2c");
	let mut bme280 = BME280::new(i2c_bus, opts.addr);
	//let mut delay = Delay;
	bme280.init(&mut Delay).expect("init bme280");

	let running = Arc::new(AtomicBool::new(true));
	let r = running.clone();

	ctrlc::set_handler(move || {
		r.store(false, Ordering::SeqCst);
	})
	.expect("Error setting Ctrl-C handler");

	tracing::info!("Starting measuring");

	while running.load(Ordering::SeqCst) {
		let result = bme280.measure(&mut Delay).expect("measurement failed");
		tracing::debug!("{:?}", result);

		metric_temperature.set(result.temperature as _);
		metric_humidity.set(result.humidity as _);
		metric_pressure.set(result.pressure as _);

		thread::sleep(Duration::from_millis(1000));
	}
}

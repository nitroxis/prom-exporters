use std::{
	sync::{
		atomic::{AtomicBool, Ordering},
		Arc,
	},
	thread,
	time::Duration,
};

use clap::Parser;
use linux_embedded_hal::serialport;
use prometheus_exporter::prometheus::{register_gauge, register_gauge_vec};
use sps30::{DeviceInfo, Sps30};
use tracing_subscriber::{fmt, prelude::*, EnvFilter};

use crate::sps30::Error;

mod sps30;

#[derive(Debug, Parser)]
struct Opts {
	/// The HTTP listen address
	#[clap(short, long, default_value = "0.0.0.0:9198")]
	listen_addr: String,

	/// The serial device path (e.g. /dev/tty0).
	device: String,
}

fn main() {
	tracing_subscriber::registry()
		.with(fmt::layer())
		.with(EnvFilter::from_default_env())
		.init();

	let opts = Opts::parse();

	prometheus_exporter::start(opts.listen_addr.parse().expect("parse listen addr"))
		.expect("failed to start prometheus exporter");

	let metric_mc = register_gauge_vec!("sps30_mc", "SPS30 mass concentration [µg / m³]", &["pm"]).expect("register metric");

	let metric_nc = register_gauge_vec!("sps30_nc", "SPS30 Number Concentration [# / cm³]", &["pm"]).expect("register metric");

	let metric_typical_particle_size =
		register_gauge!("sps30_typical_particle_size", "SPS30 typical particle size [µm]").expect("register metric");

	let metric_mc_1p0 = metric_mc.with_label_values(&["1.0"]);
	let metric_mc_2p5 = metric_mc.with_label_values(&["2.5"]);
	let metric_mc_4p0 = metric_mc.with_label_values(&["4.0"]);
	let metric_mc_10p0 = metric_mc.with_label_values(&["10.0"]);

	let metric_nc_0p5 = metric_nc.with_label_values(&["0.5"]);
	let metric_nc_1p0 = metric_nc.with_label_values(&["1.0"]);
	let metric_nc_2p5 = metric_nc.with_label_values(&["2.5"]);
	let metric_nc_4p0 = metric_nc.with_label_values(&["4.0"]);
	let metric_nc_10p0 = metric_nc.with_label_values(&["10.0"]);

	let tty = serialport::new(&opts.device, 115200)
		.flow_control(serialport::FlowControl::None)
		.parity(serialport::Parity::None)
		.data_bits(serialport::DataBits::Eight)
		.stop_bits(serialport::StopBits::One)
		.timeout(Duration::from_secs(3))
		.open()
		.expect("open tty");

	let mut sps30 = Sps30::new(tty);
	tracing::info!("Waking up...");
	match sps30.wake_up() {
		Ok(()) => {
			tracing::info!("Device woken up");
		}
		Err(Error::Status(0x43)) => {} // device already awake
		Err(e) => {
			panic!("Failed to wake up device: {e}");
		}
	}
	sps30.reset().unwrap();
	thread::sleep(Duration::from_millis(1000));

	let clean_interval = sps30.read_cleaning_interval().unwrap();
	let product_type = String::from_utf8(sps30.device_info(DeviceInfo::ProductType).unwrap()).unwrap();
	let serial_number = String::from_utf8(sps30.device_info(DeviceInfo::SerialNumber).unwrap().to_vec()).unwrap();
	let version = sps30.version().unwrap();

	tracing::info!(clean_interval, product_type, serial_number, ?version, "Device info");

	let running = Arc::new(AtomicBool::new(true));
	let r = running.clone();

	ctrlc::set_handler(move || {
		r.store(false, Ordering::SeqCst);
	})
	.expect("Error setting Ctrl-C handler");

	tracing::info!("Starting measuring");
	sps30.start_measurement().unwrap();

	while running.load(Ordering::SeqCst) {
		thread::sleep(Duration::from_millis(1000));

		if let Some(res) = sps30.read_measurement().unwrap() {
			tracing::debug!(?res, "Measurement");

			metric_mc_1p0.set(res.mc_1p0 as f64);
			metric_mc_2p5.set(res.mc_2p5 as f64);
			metric_mc_4p0.set(res.mc_4p0 as f64);
			metric_mc_10p0.set(res.mc_10p0 as f64);

			metric_nc_0p5.set(res.nc_0p5 as f64);
			metric_nc_1p0.set(res.nc_1p0 as f64);
			metric_nc_2p5.set(res.nc_2p5 as f64);
			metric_nc_4p0.set(res.nc_4p0 as f64);
			metric_nc_10p0.set(res.nc_10p0 as f64);

			metric_typical_particle_size.set(res.typical_particle_size as f64);
		}
	}

	tracing::info!("Stopping measuring");
	sps30.stop_measurement().unwrap();

	tracing::info!("Setting device to sleep...");
	sps30.sleep().unwrap();
}

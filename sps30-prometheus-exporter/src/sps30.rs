#![allow(dead_code)]

// Adapted from https://github.com/iohe/sps30

use sensirion_hdlc::{decode, encode, HDLCError, SpecialChars};
use std::io::{Read, Write};
use thiserror::Error;

/// Max characters to read for a frame detection
const MAX_BUFFER: usize = 600;

/// Errors for this crate
#[derive(Error, Debug)]
pub enum Error {
	#[error("IO")]
	IO(#[from] std::io::Error),

	#[error("HDLC")]
	Hdlc(HDLCError),

	#[error("Invalid frame")]
	InvalidFrame,

	#[error("Invalid checksum")]
	InvalidChecksum,

	#[error("Invalid response")]
	InvalidResponse,

	#[error("Error {0:#04x}")]
	Status(u8),
}

/// Types of information device holds
#[repr(u8)]
pub enum DeviceInfo {
	/// Product Name
	ProductType = 0,
	/// Serial Number
	SerialNumber = 3,
}

/// Available commands
#[repr(u8)]
pub enum CommandType {
	/// Start measurement
	StartMeasurement = 0x00,
	/// Stop measurement
	StopMeasurement = 0x01,
	/// Read measurement
	ReadMeasuredData = 0x03,
	Sleep = 0x10,
	WakeUp = 0x11,
	/// Start Fan Cleaning
	StartFanCleaning = 0x56,
	/// Read/Write Auto Cleaning Interval
	ReadWriteAutoCleaningInterval = 0x80,
	/// Device Information
	DeviceInformation = 0xD0,
	ReadVersion = 0xD1,
	ReadStatus = 0xD2,
	/// Reset
	Reset = 0xD3,
}

/// Checksum implemented as per section 4.1 from spec
fn compute_cksum(data: &[u8]) -> u8 {
	let mut cksum: u8 = 0;
	for &byte in data.iter() {
		let val: u16 = cksum as u16 + byte as u16;
		let lsb = val % 256;
		cksum = lsb as u8;
	}

	255 - cksum
}

fn verify_response(data: &[u8], cmd: CommandType) -> Result<(), Error> {
	if data.len() < 4 {
		return Err(Error::InvalidResponse);
	}

	if data[1] != cmd as u8 {
		return Err(Error::InvalidResponse);
	}

	if data[2] != 0 {
		return Err(Error::Status(data[2]));
	}

	if data[3] as usize != data.len() - 4 {
		return Err(Error::InvalidResponse);
	}

	Ok(())
}

#[derive(Debug, Clone)]
pub struct Measurement {
	pub mc_1p0: f32,
	pub mc_2p5: f32,
	pub mc_4p0: f32,
	pub mc_10p0: f32,
	pub nc_0p5: f32,
	pub nc_1p0: f32,
	pub nc_2p5: f32,
	pub nc_4p0: f32,
	pub nc_10p0: f32,
	pub typical_particle_size: f32,
}

#[derive(Debug, Clone)]
pub struct Version {
	pub firmware_major: u8,
	pub firmware_minor: u8,
	pub hardware_revision: u8,
	pub shdlc_major: u8,
	pub shdlc_minor: u8,
}

/// Sps30 driver
#[derive(Debug, Default)]
pub struct Sps30<S>
where
	S: Read + Write,
{
	serial: S,
}

// pub struct I2cInterface<I>
// where
// 	I: i2c::Write + i2c::Read,
// {
// 	i2c: I,
// 	addr: u8,
// }

// impl<I> I2cInterface<I>
// where
// 	I: i2c::Write + i2c::Read,
// {
// 	pub fn new(i2c: I, addr: u8) -> Self {
// 		Self { i2c, addr }
// 	}
// }

// fn calc_crc(data: [u8; 2]) -> u8 {
// 	let mut crc = 0xFF;

// 	for b in data {
// 		crc ^= b;
// 		for _ in 0..8 {
// 			if (crc & 0x80) != 0 {
// 				crc = (crc << 1) ^ 0x31;
// 			} else {
// 				crc <<= 1;
// 			}
// 		}
// 	}

// 	crc
// }

// impl<I> CommunicationInterface for I2cInterface<I>
// where
// 	I: i2c::Write<Error = io::Error> + i2c::Read<Error = io::Error>,
// {
// 	fn write(&mut self, command: CommandType, args: &[u8]) -> Result<(), Error> {
// 		let p = command as u16;
// 		let p = p.to_be_bytes();

// 		let mut frame = [0u8; 256];
// 		frame[0] = p[0];
// 		frame[1] = p[1];

// 		let mut i = 2;
// 		for chunk in args.chunks(2) {
// 			let data = [chunk[0], *chunk.get(1).unwrap_or(&0)];

// 			frame[i] = data[0];
// 			i += 1;

// 			frame[i] = data[1];
// 			i += 1;

// 			frame[i] = calc_crc(data);
// 			i += 1;
// 		}

// 		self.i2c.write(self.addr, &frame[..i])?;

// 		Ok(())
// 	}

// 	fn read(&mut self, command: CommandType, args: &mut [u8]) -> Result<usize, Error> {
// 		let p = command as u16;
// 		let p = p.to_be_bytes();

// 		self.i2c.write(self.addr, &p)?;

// 		let mut buf = [0u8; 3];
// 		loop {
// 			match self.i2c.read(self.addr, &mut buf) {
// 				Ok(()) => {
// 					let csum = calc_crc(buf[..2].try_into().unwrap());
// 					if csum != buf[3] {
// 						return Err(Error::InvalidChecksum);
// 					}

// 					args[..2].copy_from_slice(&buf[..2]);
// 					args = &mut args[2..];
// 				}
// 				Err() => todo!(),
// 			}
// 		}

// 		Ok(())
// 	}

// 	fn wakeup(&mut self) -> Result<(), Error> {
// 		todo!()
// 	}
// }

impl<S> Sps30<S>
where
	S: Read + Write,
{
	pub fn new(serial: S) -> Self {
		Self { serial }
	}

	fn write(&mut self, command: CommandType, args: &[u8]) -> Result<(), Error> {
		let mut buf = Vec::with_capacity(3 + args.len() + 1);
		buf.push(0x00); // Adr
		buf.push(command as u8);
		buf.push(args.len() as u8);
		buf.extend_from_slice(args);
		buf.push(compute_cksum(&buf));
		self.send_packet(&buf)?;
		Ok(())
	}

	fn read(&mut self, command: CommandType, args: &mut [u8]) -> Result<usize, Error> {
		let response = self.read_packet()?;
		let len = response[3] as usize;
		verify_response(&response, command)?;
		args[..len].copy_from_slice(&response[4..]);
		Ok(len)
	}

	fn send_packet(&mut self, data: &[u8]) -> Result<(), Error> {
		let s_chars = SpecialChars::default();
		let output = encode(data, s_chars).unwrap();
		self.serial.write_all(&output)?;
		Ok(())
	}

	fn read_packet(&mut self) -> Result<Vec<u8>, Error> {
		let mut output = Vec::<u8>::new();

		let mut seen = 0;
		let mut buf = [0u8; 1];

		while seen != 2 {
			self.serial.read_exact(&mut buf)?;
			let byte = buf[0];

			if byte == 0x7e {
				seen += 1;
			}

			if seen > 0 {
				if output.len() >= MAX_BUFFER {
					return Err(Error::InvalidFrame);
				}

				output.push(byte);
			}
		}

		let v = decode(&output, SpecialChars::default()).map_err(Error::Hdlc)?;

		let chk = v[v.len() - 1];
		let data = &v[..v.len() - 1];
		if chk != compute_cksum(data) {
			Err(Error::InvalidChecksum)
		} else {
			Ok(data.to_vec())
		}
	}

	pub fn start_measurement(&mut self) -> Result<(), Error> {
		self.write(CommandType::StartMeasurement, &[0x01, 0x03])?;
		self.read(CommandType::StartMeasurement, &mut [])?;
		Ok(())
	}

	pub fn stop_measurement(&mut self) -> Result<(), Error> {
		self.write(CommandType::StopMeasurement, &[])?;
		self.read(CommandType::StopMeasurement, &mut [])?;
		Ok(())
	}

	pub fn read_measurement(&mut self) -> Result<Option<Measurement>, Error> {
		self.write(CommandType::ReadMeasuredData, &[])?;

		let mut data = vec![0u8; 40];
		let len = self.read(CommandType::ReadMeasuredData, &mut data)?;

		if len == 40 {
			Ok(Some(Measurement {
				mc_1p0: f32::from_be_bytes(data[0..4].try_into().unwrap()),
				mc_2p5: f32::from_be_bytes(data[4..8].try_into().unwrap()),
				mc_4p0: f32::from_be_bytes(data[8..12].try_into().unwrap()),
				mc_10p0: f32::from_be_bytes(data[12..16].try_into().unwrap()),
				nc_0p5: f32::from_be_bytes(data[16..20].try_into().unwrap()),
				nc_1p0: f32::from_be_bytes(data[20..24].try_into().unwrap()),
				nc_2p5: f32::from_be_bytes(data[24..28].try_into().unwrap()),
				nc_4p0: f32::from_be_bytes(data[28..32].try_into().unwrap()),
				nc_10p0: f32::from_be_bytes(data[32..36].try_into().unwrap()),
				typical_particle_size: f32::from_be_bytes(data[36..40].try_into().unwrap()),
			}))
		} else if len == 0 {
			Ok(None)
		} else {
			Err(Error::InvalidResponse)
		}
	}

	pub fn sleep(&mut self) -> Result<(), Error> {
		self.write(CommandType::Sleep, &[])?;
		self.read(CommandType::Sleep, &mut [])?;
		Ok(())
	}

	pub fn wake_up(&mut self) -> Result<(), Error> {
		self.serial.write_all(&[0xFF])?;
		self.write(CommandType::WakeUp, &[])?;
		self.read(CommandType::WakeUp, &mut [])?;
		Ok(())
	}

	pub fn start_fan_cleaning(&mut self) -> Result<(), Error> {
		self.write(CommandType::StartFanCleaning, &[])?;
		self.read(CommandType::StartFanCleaning, &mut [])?;
		Ok(())
	}

	pub fn read_cleaning_interval(&mut self) -> Result<u32, Error> {
		self.write(CommandType::ReadWriteAutoCleaningInterval, &[0x00])?;

		let mut res = [0u8; 4];
		let n = self.read(CommandType::ReadWriteAutoCleaningInterval, &mut res)?;
		assert!(n == 4);

		Ok(u32::from_be_bytes(res))
	}

	pub fn write_cleaning_interval(&mut self, val: u32) -> Result<(), Error> {
		let mut args = Vec::with_capacity(5);
		args.push(0x00);
		args.extend_from_slice(&val.to_be_bytes());

		self.write(CommandType::ReadWriteAutoCleaningInterval, &args)?;
		self.read(CommandType::ReadWriteAutoCleaningInterval, &mut [])?;

		Ok(())
	}

	pub fn device_info(&mut self, info: DeviceInfo) -> Result<Vec<u8>, Error> {
		self.write(CommandType::DeviceInformation, &[info as u8])?;

		let mut data = vec![0u8; 33];

		let len = self.read(CommandType::DeviceInformation, &mut data)?;
		assert!(len > 0);
		assert!(data[len - 1] == 0);

		data.truncate(len - 1);
		Ok(data)
	}

	pub fn version(&mut self) -> Result<Version, Error> {
		let mut data = [0u8; 7];

		self.write(CommandType::ReadVersion, &[])?;
		self.read(CommandType::ReadVersion, &mut data)?;

		Ok(Version {
			firmware_major: data[0],
			firmware_minor: data[1],
			hardware_revision: data[3],
			shdlc_major: data[5],
			shdlc_minor: data[6],
		})
	}

	pub fn reset(&mut self) -> Result<(), Error> {
		self.write(CommandType::Reset, &[])?;
		self.read(CommandType::Reset, &mut [])?;
		Ok(())
	}
}

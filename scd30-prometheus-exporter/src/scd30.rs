// Adapted from https://github.com/sinewave-ee/scd30-rs

#![allow(dead_code)]

use crc_all::CrcAlgo;
use embedded_hal::i2c::I2c;

#[derive(Debug, Clone, Copy)]
pub enum Command {
	StartContinuousMeasurement = 0x0010,
	StopContinuousMeasurement = 0x0104,
	MeasurementInterval = 0x4600,
	GetDataReadyStatus = 0x0202,
	ReadMeasurement = 0x0300,
	AutomaticSelfCalibration = 0x5306,
	ForcedRecalibrationValue = 0x5204,
	TemperatureOffset = 0x5403,
	SetAltitude = 0x5102,
	ReadFirmwareVersion = 0xd100,
	SoftReset = 0xd304,
}

const ADDRESS: u8 = 0x61;

pub struct Scd30<T> {
	comm: T,
	address: u8,
}

#[derive(Debug)]
pub struct Measurement {
	pub co2: f32,
	pub humidity: f32,
	pub temperature: f32,
}

const CRC: CrcAlgo<u8> = CrcAlgo::<u8>::new(0x31, 8, 0xff, 0, false);

fn calculate_crc(data: &[u8]) -> u8 {
	let mut crc = 0;
	CRC.init_crc(&mut crc);
	CRC.update_crc(&mut crc, data);
	CRC.finish_crc(&crc)
}

/// See the [datasheet] for I²c parameters.
///
/// [datasheet]: https://www.sensirion.com/fileadmin/user_upload/customers/sensirion/Dokumente/9.5_CO2/Sensirion_CO2_Sensors_SCD30_Interface_Description.pdf
impl<T, E> Scd30<T>
where
	T: I2c<Error = E>,
{
	/// Returns an [Scd30] instance with the default address 0x61 shifted one place to the left.
	/// You may or may not need this bitshift depending on the byte size of
	/// your [I²c](embedded_hal::blocking::i2c) peripheral.
	pub fn new(i2c: T) -> Self {
		Scd30 {
			comm: i2c,
			address: ADDRESS,
		}
	}

	/// Returns an [Scd30] instance with the specified address.
	pub fn new_with_address(i2c: T, address: u8) -> Self {
		Scd30 { comm: i2c, address }
	}

	fn write(&mut self, command: Command, arg: Option<u16>) -> Result<(), E> {
		let c = command as u16;

		let mut buf: [u8; 5] = [(c >> 8) as u8, (c & 0xFF) as u8, 0, 0, 0];

		let len = match arg {
			Some(d) => {
				buf[2] = (d >> 8) as u8;
				buf[3] = (d & 0xFF) as u8;
				buf[4] = calculate_crc(&buf[2..4]);
				5
			}
			None => 2,
		};

		tracing::trace!(?command, data = ?&buf[..len], "write");
		self.comm.write(self.address, &buf[..len])
	}

	fn write_read(&mut self, command: Command, res: &mut [u8]) -> Result<(), E> {
		let buf = &(command as u16).to_be_bytes();
		tracing::trace!(?command, data = ?buf, "write");
		self.comm.write(self.address, &(command as u16).to_be_bytes())?;
		self.comm.read(self.address, res)?;

		//self.comm.write_read(self.address, &(command as u16).to_be_bytes(), res)?;
		tracing::trace!(?res, "read");
		Ok(())
	}

	pub fn soft_reset(&mut self) -> Result<(), E> {
		self.write(Command::SoftReset, None)
	}

	pub fn stop_measuring(&mut self) -> Result<(), E> {
		self.write(Command::StopContinuousMeasurement, None)
	}

	/// Enable or disable automatic self calibration (ASC).
	///
	/// According to the datasheet, the sensor should be active continously for at least
	/// 7 days to find the initial parameter for ASC. The sensor has to be exposed to
	/// at least 1 hour of fresh air (~400ppm CO₂) per day.
	pub fn set_automatic_calibration(&mut self, enable: bool) -> Result<(), E> {
		self.write(Command::AutomaticSelfCalibration, Some(enable as u16))
	}

	pub fn get_automatic_calibration(&mut self) -> Result<bool, E> {
		let mut buf = [0u8; 2];
		self.write_read(Command::AutomaticSelfCalibration, &mut buf)?;
		Ok(u16::from_be_bytes([buf[0], buf[1]]) != 0)
	}

	pub fn set_forced_recalibration_value(&mut self, co2: u16) -> Result<(), E> {
		self.write(Command::ForcedRecalibrationValue, Some(co2))
	}

	pub fn get_forced_recalibration_value(&mut self) -> Result<u16, E> {
		let mut buf = [0u8; 2];
		self.write_read(Command::ForcedRecalibrationValue, &mut buf)?;
		Ok(u16::from_be_bytes([buf[0], buf[1]]))
	}

	pub fn set_temperature_offset(&mut self, offset: u16) -> Result<(), E> {
		self.write(Command::TemperatureOffset, Some(offset))
	}

	pub fn get_temperature_offset(&mut self) -> Result<u16, E> {
		let mut buf = [0u8; 2];
		self.write_read(Command::TemperatureOffset, &mut buf)?;
		Ok(u16::from_be_bytes([buf[0], buf[1]]))
	}

	pub fn set_measurement_interval(&mut self, seconds: u16) -> Result<(), E> {
		self.write(Command::MeasurementInterval, Some(seconds))
	}

	pub fn get_measurement_interval(&mut self) -> Result<u16, E> {
		let mut buf = [0u8; 2];
		self.write_read(Command::MeasurementInterval, &mut buf)?;
		Ok(u16::from_be_bytes([buf[0], buf[1]]))
	}

	pub fn read_firmware_version(&mut self) -> Result<u16, E> {
		let mut buf = [0u8; 2];
		self.write_read(Command::ReadFirmwareVersion, &mut buf)?;
		Ok(u16::from_be_bytes([buf[0], buf[1]]))
	}

	/// Start measuring without mbar compensation.
	pub fn start_measuring(&mut self) -> Result<(), E> {
		self.start_measuring_with_mbar(0)
	}

	/// Start measuring with mbar (pressure) compensation.
	pub fn start_measuring_with_mbar(&mut self, pressure: u16) -> Result<(), E> {
		self.write(Command::StartContinuousMeasurement, Some(pressure))
	}

	pub fn data_ready(&mut self) -> Result<bool, E> {
		let mut buf = [0u8; 2];
		self.write_read(Command::GetDataReadyStatus, &mut buf)?;
		Ok(u16::from_be_bytes([buf[0], buf[1]]) == 1)
	}

	pub fn read(&mut self) -> Result<Option<Measurement>, E> {
		match self.data_ready() {
			Ok(true) => {
				let mut buf = [0u8; 6 * 3];

				self.write_read(Command::ReadMeasurement, &mut buf)?;

				Ok(Some(Measurement {
					co2: f32::from_bits(u32::from_be_bytes([buf[0], buf[1], buf[3], buf[4]])),
					temperature: f32::from_bits(u32::from_be_bytes([buf[6], buf[7], buf[9], buf[10]])),
					humidity: f32::from_bits(u32::from_be_bytes([buf[12], buf[13], buf[15], buf[16]])),
				}))
			}
			Ok(false) => Ok(None),
			Err(e) => Err(e),
		}
	}
}

use std::{
	sync::{
		atomic::{AtomicBool, Ordering},
		Arc,
	},
	thread,
	time::Duration,
};

use clap::Parser;
use linux_embedded_hal::I2cdev;
use prometheus_exporter::prometheus::register_gauge;
use scd30::Scd30;
use signal_hook::consts::{SIGINT, SIGTERM, SIGUSR1};
use tracing_subscriber::{fmt, prelude::*, EnvFilter};

mod scd30;

#[derive(Debug, Parser)]
struct Opts {
	/// The HTTP listen address
	#[clap(short, long, default_value = "0.0.0.0:9197")]
	listen_addr: String,

	/// The I²C address.
	#[clap(short, long, default_value = "97")]
	addr: u8,

	/// Issue a stop-measuring command before starting measurement.
	#[clap(long)]
	stop: bool,

	/// Issue a soft-reset before starting measurement.
	#[clap(long)]
	reset: bool,

	/// Whether ASC (automatic self-calibration) should be used.
	#[clap(long)]
	asc: Option<bool>,

	/// The FRC (forced re-calibration) value.
	#[clap(long)]
	frc: Option<u16>,

	/// The temperature offset (in °C) to use.
	#[clap(long)]
	temp_offset: Option<f32>,

	/// The pressure (in mbar) to use.
	#[clap(long)]
	mbar: Option<u16>,

	/// The measurement interval (in seconds).
	#[clap(long)]
	interval: Option<u16>,

	/// The I²C device path (e.g. /dev/i2c-0).
	device: String,
}

fn main() {
	tracing_subscriber::registry()
		.with(fmt::layer())
		.with(EnvFilter::from_default_env())
		.init();

	let opts = Opts::parse();

	prometheus_exporter::start(opts.listen_addr.parse().expect("parse listen addr"))
		.expect("failed to start prometheus exporter");

	let metric_co2 = register_gauge!("scd30_co2", "SCD30 CO2 measurement in ppm").expect("register metric");
	let metric_humidity = register_gauge!("scd30_humidity", "SCD30 humidity in percent").expect("register metric");
	let metric_temperature = register_gauge!("scd30_temperature", "SCD30 temperature in celsius").expect("register metric");

	let i2c_bus = I2cdev::new(&opts.device).expect("open i2c");
	let mut scd30 = Scd30::new_with_address(i2c_bus, opts.addr);

	if opts.stop {
		let res = scd30.stop_measuring();
		tracing::info!(?res, "Stop measuring");
		thread::sleep(Duration::from_secs(1));
	}

	if opts.reset {
		tracing::info!("Soft reset");
		scd30.soft_reset().expect("soft reset");
		thread::sleep(Duration::from_secs(2));
	}

	if let Some(asc) = opts.asc {
		tracing::info!(asc, "Setting ASC");
		scd30.set_automatic_calibration(asc).expect("set asc");
	}

	if let Some(frc) = opts.frc {
		tracing::info!(frc, "Setting FRC");
		scd30.set_forced_recalibration_value(frc).expect("set frc");
	}

	if let Some(temp_offset) = opts.temp_offset {
		let temp_offset = (temp_offset * 100f32) as u16;
		tracing::info!(temp_offset, "Setting temperature offset");
		scd30.set_temperature_offset(temp_offset).expect("set temperature offset");
	}

	if let Some(interval) = opts.interval {
		tracing::info!(interval, "Setting measurement interval");
		scd30.set_measurement_interval(interval).expect("set interval");
	}

	let fw = scd30.read_firmware_version().expect("read fw ver");
	let asc = scd30.get_automatic_calibration().expect("get asc");
	let frc = scd30.get_forced_recalibration_value().expect("get frc");
	let interval = scd30.get_measurement_interval().expect("get interval");
	let temp_offset = scd30.get_temperature_offset().expect("get temperature offset");
	tracing::info!(fw, asc, frc, temp_offset, interval, "Current configuration");

	let exit = Arc::new(AtomicBool::new(false));
	let do_frc = Arc::new(AtomicBool::new(false));

	signal_hook::flag::register(SIGTERM, exit.clone()).expect("SIGTERM handler");
	signal_hook::flag::register(SIGINT, exit.clone()).expect("SIGINT handler");
	signal_hook::flag::register(SIGUSR1, do_frc.clone()).expect("SIGUSR1 handler");

	if let Some(mbar) = opts.mbar {
		tracing::info!(mbar, "Starting measuring");
		scd30.start_measuring_with_mbar(mbar).expect("start measuring");
	} else {
		tracing::info!("Starting measuring");
		scd30.start_measuring().expect("start measuring");
	}

	'mainloop: while !exit.load(Ordering::Relaxed) {
		if do_frc.swap(false, Ordering::Relaxed) {
			tracing::info!("Setting FRC to 400ppm");
			scd30.set_forced_recalibration_value(400).expect("set frc");
			thread::sleep(Duration::from_millis(100));
			continue;
		}

		while !scd30.data_ready().expect("data ready") {
			tracing::debug!("data not ready");

			if exit.load(Ordering::Relaxed) {
				break 'mainloop;
			}

			thread::sleep(Duration::from_millis(1000));
		}

		let res = scd30.read().expect("read");
		if let Some(res) = res {
			tracing::debug!(?res, "Measurement");

			metric_co2.set(res.co2 as f64);
			metric_humidity.set(res.humidity as f64);
			metric_temperature.set(res.temperature as f64);
		}
		thread::sleep(Duration::from_millis(1000));
	}

	tracing::info!("Stopping measuring");
	scd30.stop_measuring().expect("stop measuring");
}

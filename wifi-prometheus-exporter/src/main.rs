use std::{
	collections::HashSet,
	string::FromUtf8Error,
	sync::{
		atomic::{AtomicBool, Ordering},
		Arc,
	},
	thread,
	time::{Duration, Instant},
};

use clap::Parser;
use lazy_static::lazy_static;
use neli_wifi::Socket;
use prometheus_exporter::prometheus::{register_gauge_vec, register_int_gauge_vec, GaugeVec, IntGaugeVec};
use tracing_subscriber::{fmt, prelude::*, EnvFilter};

#[derive(Debug, Parser)]
struct Opts {
	/// The HTTP listen address
	#[clap(short, long, default_value = "0.0.0.0:9190")]
	listen_addr: String,

	/// Update interval in seconds
	#[clap(short, long, default_value = "1")]
	interval: f64,
}

const INTERFACE_LABELS: &[&str] = &["if_index", "if_phy", "if_mac", "if_name"];
const STATION_LABELS: &[&str] = &["if_index", "if_phy", "if_mac", "if_name", "bssid"];

lazy_static! {
	static ref WIFI_FREQUENCY: IntGaugeVec = register_int_gauge_vec!(
		"wifi_interface_frequency",
		"WiFi interface frequency in MHz",
		INTERFACE_LABELS
	)
	.unwrap();
	static ref WIFI_CHANNEL: IntGaugeVec =
		register_int_gauge_vec!("wifi_interface_channel", "WiFi interface channel", INTERFACE_LABELS).unwrap();
	static ref WIFI_POWER: IntGaugeVec =
		register_int_gauge_vec!("wifi_interface_power", "WiFi interface power in dBm", INTERFACE_LABELS).unwrap();
	static ref WIFI_STATION_RX_BYTES: IntGaugeVec =
		register_int_gauge_vec!("wifi_station_rx_bytes", "WiFi station bytes received", STATION_LABELS).unwrap();
	static ref WIFI_STATION_RX_PACKETS: IntGaugeVec =
		register_int_gauge_vec!("wifi_station_rx_packets", "WiFi station packets received", STATION_LABELS).unwrap();
	static ref WIFI_STATION_TX_BYTES: IntGaugeVec =
		register_int_gauge_vec!("wifi_station_tx_bytes", "WiFi station bytes transmitted", STATION_LABELS).unwrap();
	static ref WIFI_STATION_TX_PACKETS: IntGaugeVec =
		register_int_gauge_vec!("wifi_station_tx_packets", "WiFi station packets transmitted", STATION_LABELS).unwrap();
	static ref WIFI_STATION_TX_RETRIES: IntGaugeVec =
		register_int_gauge_vec!("wifi_station_tx_retries", "WiFi station transmit retries", STATION_LABELS).unwrap();
	static ref WIFI_STATION_TX_FAILED: IntGaugeVec =
		register_int_gauge_vec!("wifi_station_tx_failed", "WiFi station transmit failures", STATION_LABELS).unwrap();
	static ref WIFI_STATION_RX_DROP_MISC: IntGaugeVec =
		register_int_gauge_vec!("wifi_station_rx_drop_misc", "WiFi station misc receive drops", STATION_LABELS).unwrap();
	static ref WIFI_STATION_SIGNAL: IntGaugeVec =
		register_int_gauge_vec!("wifi_station_signal", "WiFi station signal strength in dBm", STATION_LABELS).unwrap();
	static ref WIFI_STATION_SIGNAL_AVG: IntGaugeVec = register_int_gauge_vec!(
		"wifi_station_signal_avg",
		"WiFi station average signal strength in dBm",
		STATION_LABELS
	)
	.unwrap();
	static ref WIFI_STATION_TX_BITRATE: GaugeVec = register_gauge_vec!(
		"wifi_station_tx_bitrate",
		"WiFi station transmit bitrate in bit/s",
		STATION_LABELS
	)
	.unwrap();
	static ref WIFI_STATION_TX_DURATION: GaugeVec = register_gauge_vec!(
		"wifi_station_tx_duration",
		"WiFi station transmit duration in s",
		STATION_LABELS
	)
	.unwrap();
	static ref WIFI_STATION_RX_BITRATE: GaugeVec = register_gauge_vec!(
		"wifi_station_rx_bitrate",
		"WiFi station receive bitrate in bit/s",
		STATION_LABELS
	)
	.unwrap();
	static ref WIFI_STATION_RX_DURATION: GaugeVec = register_gauge_vec!(
		"wifi_station_rx_duration",
		"WiFi station receive duration in s",
		STATION_LABELS
	)
	.unwrap();
	static ref WIFI_STATION_CONNECTED_TIME: GaugeVec = register_gauge_vec!(
		"wifi_station_connected_time",
		"WiFi station connected time in s",
		STATION_LABELS
	)
	.unwrap();
}

fn format_mac(mac: &[u8]) -> String {
	format!(
		"{:02x}:{:02x}:{:02x}:{:02x}:{:02x}:{:02x}",
		mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]
	)
}

fn get_str(bytes: Vec<u8>) -> Result<String, FromUtf8Error> {
	let mut s = String::from_utf8(bytes)?;
	if s.ends_with('\0') {
		s.truncate(s.len() - 1);
	}

	Ok(s)
}

fn main() -> anyhow::Result<()> {
	tracing_subscriber::registry()
		.with(fmt::layer())
		.with(EnvFilter::from_default_env())
		.init();

	let opts = Opts::parse();

	prometheus_exporter::start(opts.listen_addr.parse().expect("parse listen addr"))
		.expect("failed to start prometheus exporter");

	let running = Arc::new(AtomicBool::new(true));
	let r = running.clone();

	ctrlc::set_handler(move || {
		r.store(false, Ordering::SeqCst);
	})
	.expect("Error setting Ctrl-C handler");

	tracing::info!("Starting data collection");

	let mut socket = Socket::connect()?;

	let mut if_labels_prev = HashSet::new();
	let mut sta_labels_prev = HashSet::new();

	let interval = Duration::from_secs_f64(opts.interval);

	while running.load(Ordering::SeqCst) {
		let next = Instant::now() + interval;

		let mut if_labels = HashSet::new();
		let mut sta_labels = HashSet::new();

		for interface in socket.get_interfaces_info()? {
			let if_mac = if let Some(mac) = &interface.mac {
				format_mac(mac)
			} else {
				String::new()
			};

			let if_name = interface.name.map(get_str).unwrap_or_else(|| Ok(String::default()))?;

			let mut labels = vec![
				interface.index.map(|i| i.to_string()).unwrap_or_default(),
				interface.phy.map(|i| i.to_string()).unwrap_or_default(),
				if_mac,
				if_name,
			];

			let labels_ref = labels.iter().map(|s| s.as_str()).collect::<Vec<_>>();

			// Update interface.
			if let Some(v) = interface.frequency {
				WIFI_FREQUENCY.with_label_values(&labels_ref).set(v as _);
			}

			if let Some(v) = interface.channel {
				WIFI_CHANNEL.with_label_values(&labels_ref).set(v as _);
			}

			if let Some(v) = interface.power {
				WIFI_POWER.with_label_values(&labels_ref).set(v as _);
			}

			// Update stations.
			if let Some(index) = interface.index {
				let stations = socket.get_station_info(index)?;

				for station in stations {
					let bssid = if let Some(mac) = &station.bssid {
						format_mac(mac)
					} else {
						String::new()
					};

					labels.push(bssid);

					sta_labels.insert(labels.clone());

					let labels_ref = labels.iter().map(|s| s.as_str()).collect::<Vec<_>>();

					if let Some(v) = station.rx_bytes {
						WIFI_STATION_RX_BYTES.with_label_values(&labels_ref).set(v as _);
					}

					if let Some(v) = station.rx_packets {
						WIFI_STATION_RX_PACKETS.with_label_values(&labels_ref).set(v as _);
					}

					if let Some(v) = station.tx_bytes {
						WIFI_STATION_TX_BYTES.with_label_values(&labels_ref).set(v as _);
					}

					if let Some(v) = station.tx_packets {
						WIFI_STATION_TX_PACKETS.with_label_values(&labels_ref).set(v as _);
					}

					if let Some(v) = station.tx_retries {
						WIFI_STATION_TX_RETRIES.with_label_values(&labels_ref).set(v as _);
					}

					if let Some(v) = station.tx_failed {
						WIFI_STATION_TX_FAILED.with_label_values(&labels_ref).set(v as _);
					}

					if let Some(v) = station.rx_drop_misc {
						WIFI_STATION_RX_DROP_MISC.with_label_values(&labels_ref).set(v as _);
					}

					if let Some(v) = station.signal {
						WIFI_STATION_SIGNAL.with_label_values(&labels_ref).set(v as _);
					}

					if let Some(v) = station.average_signal {
						WIFI_STATION_SIGNAL_AVG.with_label_values(&labels_ref).set(v as _);
					}

					if let Some(v) = station.tx_bitrate {
						WIFI_STATION_TX_BITRATE
							.with_label_values(&labels_ref)
							.set(v as f64 * 100.0 * 1024.0);
					}

					if let Some(v) = station.tx_duration {
						WIFI_STATION_TX_DURATION
							.with_label_values(&labels_ref)
							.set(v as f64 / 1_000_000.0);
					}

					if let Some(v) = station.rx_bitrate {
						WIFI_STATION_RX_BITRATE
							.with_label_values(&labels_ref)
							.set(v as f64 * 100.0 * 1024.0);
					}

					if let Some(v) = station.rx_duration {
						WIFI_STATION_RX_DURATION
							.with_label_values(&labels_ref)
							.set(v as f64 / 1_000_000.0);
					}

					if let Some(v) = station.connected_time {
						WIFI_STATION_CONNECTED_TIME.with_label_values(&labels_ref).set(v as f64);
					}

					labels.truncate(4);
				}
			}

			if_labels.insert(labels);
		}

		// TODO: can this ⬇ be done better?

		// Remove interface that have vanished.
		for labels in if_labels_prev.difference(&if_labels) {
			let labels_ref = labels.iter().map(|s| s.as_str()).collect::<Vec<_>>();
			let _ = WIFI_FREQUENCY.remove_label_values(&labels_ref);
			let _ = WIFI_CHANNEL.remove_label_values(&labels_ref);
			let _ = WIFI_POWER.remove_label_values(&labels_ref);
		}

		// Remove stations that have vanished.
		for labels in sta_labels_prev.difference(&sta_labels) {
			let labels_ref = labels.iter().map(|s| s.as_str()).collect::<Vec<_>>();
			let _ = WIFI_STATION_RX_BYTES.remove_label_values(&labels_ref);
			let _ = WIFI_STATION_RX_PACKETS.remove_label_values(&labels_ref);
			let _ = WIFI_STATION_TX_BYTES.remove_label_values(&labels_ref);
			let _ = WIFI_STATION_TX_PACKETS.remove_label_values(&labels_ref);
			let _ = WIFI_STATION_TX_RETRIES.remove_label_values(&labels_ref);
			let _ = WIFI_STATION_TX_FAILED.remove_label_values(&labels_ref);
			let _ = WIFI_STATION_RX_DROP_MISC.remove_label_values(&labels_ref);
			let _ = WIFI_STATION_SIGNAL.remove_label_values(&labels_ref);
			let _ = WIFI_STATION_SIGNAL_AVG.remove_label_values(&labels_ref);
			let _ = WIFI_STATION_TX_BITRATE.remove_label_values(&labels_ref);
			let _ = WIFI_STATION_TX_DURATION.remove_label_values(&labels_ref);
			let _ = WIFI_STATION_RX_BITRATE.remove_label_values(&labels_ref);
			let _ = WIFI_STATION_RX_DURATION.remove_label_values(&labels_ref);
			let _ = WIFI_STATION_CONNECTED_TIME.remove_label_values(&labels_ref);
		}

		sta_labels_prev = sta_labels;
		if_labels_prev = if_labels;

		let now = Instant::now();
		if next > now {
			thread::sleep(next - now);
		}
	}

	Ok(())
}

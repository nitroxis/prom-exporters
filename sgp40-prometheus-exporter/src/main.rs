use std::{
	sync::{
		atomic::{AtomicBool, Ordering},
		Arc,
	},
	thread,
	time::Duration,
};

use clap::Parser;
use linux_embedded_hal::{Delay, I2cdev};
use prometheus_exporter::prometheus::register_gauge;
use sgp40::Sgp40;
use tracing_subscriber::{fmt, prelude::*, EnvFilter};

#[derive(Debug, Parser)]
struct Opts {
	/// The HTTP listen address
	#[clap(short, long, default_value = "0.0.0.0:9195")]
	listen_addr: String,

	/// The I²C device path (e.g. /dev/i2c-0).
	device: String,

	/// The I²C address.
	#[clap(short, long, default_value = "89")]
	addr: u8,
}

fn main() {
	tracing_subscriber::registry()
		.with(fmt::layer())
		.with(EnvFilter::from_default_env())
		.init();

	let opts = Opts::parse();

	prometheus_exporter::start(opts.listen_addr.parse().expect("parse listen addr"))
		.expect("failed to start prometheus exporter");

	let metric_voc = register_gauge!("sgp40_voc_index", "SGP40 VOC index").expect("register metric");

	let i2c_bus = I2cdev::new(&opts.device).expect("open i2c");
	let mut sgp40 = Sgp40::new(i2c_bus, opts.addr, Delay);

	let running = Arc::new(AtomicBool::new(true));
	let r = running.clone();

	ctrlc::set_handler(move || {
		r.store(false, Ordering::SeqCst);
	})
	.expect("Error setting Ctrl-C handler");

	tracing::info!("Device serial: {:?}", sgp40.serial().expect("read serial"));

	tracing::info!("Warming up");

	for _ in 1..45 {
		sgp40.measure_voc_index().unwrap();
	}

	tracing::info!("Starting measuring");

	while running.load(Ordering::SeqCst) {
		let voc = sgp40.measure_voc_index().expect("measure");

		metric_voc.set(voc as _);

		tracing::debug!("voc: {}", voc);
		thread::sleep(Duration::from_millis(1000));
	}
}

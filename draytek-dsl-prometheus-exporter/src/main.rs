use std::io::ErrorKind;
use std::path::Path;
use std::time::Duration;
use std::{net::SocketAddr, path::PathBuf, sync::Arc};

use anyhow::bail;
use bytes::BytesMut;
use chrono::Timelike;
use clap::{Parser, ValueEnum};
use futures::TryStreamExt;
use futures::{Sink, SinkExt};
use lazy_static::lazy_static;
use telnet::TelnetCodec;
use tokio::time::{sleep, Instant};
use tokio::{
	fs::OpenOptions,
	io::{AsyncBufRead, AsyncBufReadExt},
	net::TcpStream,
	pin,
	sync::Mutex,
	time::sleep_until,
};
use tokio_util::{
	codec::{FramedRead, FramedWrite},
	io::StreamReader,
};
use tracing_subscriber::{fmt, prelude::*, EnvFilter};

use crate::diag::{parse_bins, parse_delt, write_bins_csv, write_delt_csv};
use crate::{metrics::*, telnet::TelnetEvent};

mod diag;
mod metrics;
mod telnet;

#[derive(Debug, Clone, ValueEnum)]
enum DiagOpts {
	Bins,
	Hlog,
	Qln,
	Snr,
}

#[derive(Debug, Parser)]
struct Opts {
	/// The HTTP listen address
	#[clap(short, long, default_value = "0.0.0.0:9193")]
	listen_addr: String,

	/// Admin panel / telnet user name
	#[clap(short, long, default_value = "admin")]
	user: String,

	/// Admin panel / telnet password
	#[clap(short, long)]
	password: String,

	/// The telnet address (e.g. 192.168.1.1:23)
	#[clap(default_value = "192.168.1.1:23")]
	host: SocketAddr,

	/// Status polling interval
	#[clap(short, long, default_value = "5")]
	interval: f64,

	/// Query the specified diagnostics into a directory at a regular interval.
	#[clap(short, long)]
	diag: Vec<DiagOpts>,

	/// Save output of diagnostic queries into the specified directory.
	#[clap(long, default_value = ".")]
	diag_dir: PathBuf,

	/// Interval of diagnostics.
	#[clap(long, default_value = "60")]
	diag_interval: f64,

	/// Create a symlink to the latest file.
	#[clap(long)]
	diag_link_latest: bool,
}

lazy_static! {
	static ref STATUS_METRICS: [Metric; 10] = [
		DoubleRegexMetric::new_ds_us(
			"rate_actual",
			"Actual bits per second",
			r"DS Actual Rate\s*:\s*(\d+).+US Actual Rate\s*:\s*(\d+)",
			DataType::Int
		)
		.into(),
		DoubleRegexMetric::new_ds_us(
			"rate_attainable",
			"Attainable bits per second",
			r"DS Attainable Rate\s*:\s*(\d+).+US Attainable Rate\s*:\s*(\d+)",
			DataType::Int
		)
		.into(),
		DoubleRegexMetric::new_ds_us(
			"interleave_depth",
			"Interleave depth",
			r"DS Interleave Depth\s*:\s*(\d+).+US Interleave Depth\s*:\s*(\d+)",
			DataType::Int
		)
		.into(),
		DoubleRegexMetric::new_ds_us(
			"actual_psd",
			"Power spectrum density",
			r"DS actual PSD\s*:\s*(-?\d+\.\s*-?\d+).+US actual PSD\s*:\s*(-?\d+\.\s*-?\d+)",
			DataType::Float
		)
		.into(),
		DoubleRegexMetric::new_ne_fe(
			"current_attenuation",
			"Current attenuation",
			r"(?s)NE Current Attenuation\s*:\s*(\d+).+Far Current Attenuation\s*:\s*(\d+)",
			DataType::Int
		)
		.into(),
		DoubleRegexMetric::new_ne_fe(
			"snr_margin",
			"SNR margin",
			r"(?s)Cur SNR Margin\s*:\s*(\d+).+Far SNR Margin\s*:\s*(\d+)",
			DataType::Int
		)
		.into(),
		DoubleRegexMetric::new_ne_fe(
			"crc_count",
			"CRC error count",
			r"NE CRC Count\s*:\s*(\d+).+FE CRC Count\s*:\s*(\d+)",
			DataType::Int
		)
		.into(),
		DoubleRegexMetric::new_ne_fe(
			"es_count",
			"Errored seconds count",
			r"NE ES Count\s*:\s*(\d+).+FE  ES Count\s*:\s*(\d+)",
			DataType::Int
		)
		.into(),
		RegexMetric::new(
			"xdsl_reset_times",
			"XDSL resets",
			r"Xdsl Reset Times\s*:\s*(\d+)",
			DataType::Int
		)
		.into(),
		RegexMetric::new(
			"xdsl_link_times",
			"XDSL links",
			r"Xdsl Link  Times\s*:\s*(\d+)",
			DataType::Int
		)
		.into(),
	];
	static ref STATUS_MORE_METRICS: [Metric; 28] = [
		DoubleRegexMetric::new_ne_fe_more("Trellis").into(),
		DoubleRegexMetric::new_ne_fe_more("Bitswap").into(),
		DoubleRegexMetric::new_ne_fe_more("ReTxEnable").into(),
		DoubleRegexMetric::new_ne_fe_more("VirtualNoise").into(),
		DoubleRegexMetric::new_ne_fe_more("20BitSupport").into(),
		DoubleRegexMetric::new_ne_fe_more("LatencyPath").into(),
		DoubleRegexMetric::new_ne_fe_more("LOS").into(),
		DoubleRegexMetric::new_ne_fe_more("LOF").into(),
		DoubleRegexMetric::new_ne_fe_more("LPR").into(),
		DoubleRegexMetric::new_ne_fe_more("LOM").into(),
		DoubleRegexMetric::new_ne_fe_more("SosSuccess").into(),
		DoubleRegexMetric::new_ne_fe_more("NCD").into(),
		DoubleRegexMetric::new_ne_fe_more("LCD").into(),
		DoubleRegexMetric::new_ne_fe_more("FECS").into(),
		DoubleRegexMetric::new_ne_fe_more("ES").into(),
		DoubleRegexMetric::new_ne_fe_more("SES").into(),
		DoubleRegexMetric::new_ne_fe_more("LOSS").into(),
		DoubleRegexMetric::new_ne_fe_more("UAS").into(),
		DoubleRegexMetric::new_ne_fe_more("HECError").into(),
		DoubleRegexMetric::new_ne_fe_more("CRC").into(),
		DoubleRegexMetric::new_ne_fe_more("RsCorrection").into(),
		DoubleRegexMetric::new_ne_fe_more("INP").into(),
		DoubleRegexMetric::new_ne_fe_more("InterleaveDelay").into(),
		DoubleRegexMetric::new_ne_fe_more("NFEC").into(),
		DoubleRegexMetric::new_ne_fe_more("RFEC").into(),
		DoubleRegexMetric::new_ne_fe_more("LSYMB").into(),
		DoubleRegexMetric::new_ne_fe_more("INTLVBLOCK").into(),
		RegexMetric::new("aelem", "AELEM", r"AELEM\s*:\s*(\d+)", DataType::Int).into(),
	];
}

fn update_metrics(metrics: &[Metric], text: &str) {
	for metric in metrics.iter() {
		match metric.update(text) {
			Ok(true) => {}
			Ok(false) => {
				tracing::warn!("Metric didn't match {}", metric.name());
			}
			Err(e) => {
				tracing::warn!("Failed to update metric {}: {e:#}", metric.name());
			}
		}
	}
}

async fn expect<S>(source: &mut S, delimiter: &[u8]) -> anyhow::Result<String>
where
	S: AsyncBufRead + Unpin,
{
	let find = async {
		let mut buf = Vec::new();
		let &end = delimiter.last().unwrap();

		loop {
			let n = source.read_until(end, &mut buf).await?;
			if n == 0 {
				bail!("Search string not found");
			}

			if buf.ends_with(delimiter) {
				drop(buf.drain((buf.len() - delimiter.len())..));
				let res = String::from_utf8(buf)?;
				tracing::trace!(res, "Response");
				return Ok(res);
			}
		}
	};

	let timeout = tokio::time::sleep(Duration::from_secs(3));
	let delim = String::from_utf8_lossy(delimiter);
	tokio::select! {
		res = find => {
			res
		}
		() = timeout => {
			bail!("Timeout waiting for response from server: '{delim}'");
		}
	}
}

async fn send<S>(sink: &mut S, input: &str) -> Result<(), S::Error>
where
	S: Sink<TelnetEvent> + Unpin,
{
	tracing::debug!(input, "Sending");

	let mut bytes = BytesMut::with_capacity(input.len() + 2);
	bytes.extend_from_slice(input.as_bytes());
	bytes.extend_from_slice(&[13, 10]);

	let ev = TelnetEvent::Data(bytes.freeze());
	sink.send(ev).await
}

struct Telnet<R, W> {
	reader: R,
	writer: W,
}

async fn force_symlink(src: impl AsRef<Path>, dst: impl AsRef<Path>) -> std::io::Result<()> {
	match tokio::fs::symlink(&src, &dst).await {
		Ok(()) => Ok(()),
		Err(e) => {
			if e.kind() == ErrorKind::AlreadyExists {
				tokio::fs::remove_file(&dst).await?;
				tokio::fs::symlink(src, dst).await
			} else {
				Err(e)
			}
		}
	}
}

#[tokio::main(flavor = "current_thread")]
async fn main() -> anyhow::Result<()> {
	tracing_subscriber::registry()
		.with(fmt::layer())
		.with(EnvFilter::from_default_env())
		.init();

	let opts = Opts::parse();

	prometheus_exporter::start(opts.listen_addr.parse().expect("parse listen addr"))
		.expect("failed to start prometheus exporter");

	let mut conn = TcpStream::connect(opts.host).await?;
	let (tcp_rx, tcp_tx) = conn.split();

	let reader = FramedRead::new(tcp_rx, TelnetCodec::default());
	let reader = StreamReader::new(reader.try_filter_map(|ev| async {
		if let TelnetEvent::Data(bytes) = ev {
			Ok(Some(bytes))
		} else {
			Ok(None)
		}
	}));
	let writer = FramedWrite::new(tcp_tx, TelnetCodec::default());

	pin!(reader);
	pin!(writer);

	tracing::info!("Logging in...");
	expect(&mut reader, b"Account:").await?;
	send(&mut writer, &opts.user).await?;

	expect(&mut reader, b"Password:").await?;
	send(&mut writer, &opts.password).await?;

	expect(&mut reader, b"Vigor>").await?;
	tracing::info!("Login successful");

	let conn = Arc::new(Mutex::new(Telnet { reader, writer }));

	let conn2 = conn.clone();
	let status = async {
		let interval = Duration::from_secs_f64(opts.interval);
		let mut next = Instant::now();

		loop {
			{
				let mut telnet = conn2.lock().await;
				tracing::debug!("Updating status");

				send(&mut telnet.writer, "vdsl status").await?;
				let res = expect(&mut telnet.reader, b"Vigor>").await?;
				update_metrics(STATUS_METRICS.as_ref(), &res);

				send(&mut telnet.writer, "vdsl status more").await?;
				let res = expect(&mut telnet.reader, b"Vigor>").await?;
				update_metrics(STATUS_MORE_METRICS.as_ref(), &res);

				tracing::debug!("Status updated");
			}

			next += interval;
			sleep_until(next).await;
		}

		#[allow(unreachable_code)]
		Result::Ok::<(), anyhow::Error>(())
	};

	let conn2 = conn.clone();
	let diag = async {
		// Wait for full minute (hh:mm:00).
		let now = chrono::Local::now();
		let s = now.second() as u64;
		if s > 0 {
			sleep(Duration::from_secs(60 - s)).await;
		}

		let interval = Duration::from_secs_f64(opts.diag_interval);
		let mut next = Instant::now();

		loop {
			{
				let mut telnet = conn2.lock().await;

				let now = chrono::Local::now();
				let basename = now.format("%Y%m%d-%H%M%S");

				for diag in opts.diag.iter() {
					match diag {
						DiagOpts::Bins => {
							tracing::debug!("Saving bins");

							send(&mut telnet.writer, "vdsl showbins").await?;
							let text = expect(&mut telnet.reader, b"Vigor>").await?;
							let bins = parse_bins(&text);
							let file_name = format!("{basename}.bins.ds.csv");
							let mut file = OpenOptions::new()
								.create(true)
								.write(true)
								.truncate(true)
								.open(opts.diag_dir.join(&file_name))
								.await?;
							write_bins_csv(&bins, &mut file).await?;

							if opts.diag_link_latest {
								force_symlink(file_name, opts.diag_dir.join("latest.bins.ds.csv")).await?;
							}

							send(&mut telnet.writer, "vdsl showbins up").await?;
							let text = expect(&mut telnet.reader, b"Vigor>").await?;
							let bins = parse_bins(&text);
							let file_name = format!("{basename}.bins.us.csv");
							let mut file = OpenOptions::new()
								.create(true)
								.write(true)
								.truncate(true)
								.open(opts.diag_dir.join(&file_name))
								.await?;
							write_bins_csv(&bins, &mut file).await?;

							if opts.diag_link_latest {
								force_symlink(file_name, opts.diag_dir.join("latest.bins.us.csv")).await?;
							}

							tracing::debug!("Bins saved");
						}
						DiagOpts::Hlog => {
							tracing::debug!("Saving hlog");

							send(&mut telnet.writer, "vdsl status hlog").await?;
							let text = expect(&mut telnet.reader, b"Vigor>").await?;
							let bins = parse_delt(&text);
							let file_name = format!("{basename}.hlog.csv");
							let mut file = OpenOptions::new()
								.create(true)
								.write(true)
								.truncate(true)
								.open(opts.diag_dir.join(&file_name))
								.await?;
							write_delt_csv(&bins, &mut file).await?;

							if opts.diag_link_latest {
								force_symlink(file_name, opts.diag_dir.join("latest.hlog.csv")).await?;
							}

							tracing::debug!("hlog saved");
						}
						DiagOpts::Qln => {
							tracing::debug!("Saving qln");

							send(&mut telnet.writer, "vdsl status qln").await?;
							let text = expect(&mut telnet.reader, b"Vigor>").await?;
							let bins = parse_delt(&text);
							let file_name = format!("{basename}.qln.csv");
							let mut file = OpenOptions::new()
								.create(true)
								.write(true)
								.truncate(true)
								.open(opts.diag_dir.join(&file_name))
								.await?;
							write_delt_csv(&bins, &mut file).await?;

							if opts.diag_link_latest {
								force_symlink(file_name, opts.diag_dir.join("latest.qln.csv")).await?;
							}

							tracing::debug!("qln saved");
						}
						DiagOpts::Snr => {
							tracing::debug!("Saving snr");

							send(&mut telnet.writer, "vdsl status snr").await?;
							let text = expect(&mut telnet.reader, b"Vigor>").await?;
							let bins = parse_delt(&text);
							let file_name = format!("{basename}.snr.csv");
							let mut file = OpenOptions::new()
								.create(true)
								.write(true)
								.truncate(true)
								.open(opts.diag_dir.join(&file_name))
								.await?;
							write_delt_csv(&bins, &mut file).await?;

							if opts.diag_link_latest {
								force_symlink(file_name, opts.diag_dir.join("latest.snr.csv")).await?;
							}

							tracing::debug!("snr saved");
						}
					}
				}

				tracing::debug!("Bins saved");
			}

			next += interval;
			sleep_until(next).await;
		}

		#[allow(unreachable_code)]
		Result::Ok::<(), anyhow::Error>(())
	};

	tokio::try_join!(status, diag)?;

	Ok(())
}

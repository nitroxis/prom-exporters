use std::collections::HashMap;
use std::fmt::Write;

use lazy_static::lazy_static;
use regex::Regex;
use tokio::io::{AsyncWrite, AsyncWriteExt};

lazy_static! {
	static ref BIN_REGEX: Regex = Regex::new(r"(?m)\s*(\-?\d+)\s+(\-?\d+)\s+(\-?\d+)\s+(\-?\d+)\s*($|\*)").unwrap();
	static ref DELT_REGEX: Regex = Regex::new(r"(?m)^\s*bin=\s*(\d+):\s*([\-\d]*)\s*,\s*([\-\d]*)\s*,\s*([\-\d]*)\s*,\s*([\-\d]*)\s*,\s*([\-\d]*)\s*,\s*([\-\d]*)\s*,\s*([\-\d]*)\s*,\s*([\-\d]*)\s*,\s*$").unwrap();
}

#[derive(Debug)]
pub struct Bin {
	pub snr: i32,
	pub gain: i32,
	pub bits: u32,
}

pub fn parse_delt(text: &str) -> HashMap<u32, i32> {
	let mut res = HashMap::new();

	for caps in DELT_REGEX.captures_iter(text) {
		let n: u32 = caps.get(1).unwrap().as_str().parse().unwrap();

		for i in 0..8 {
			let value = caps.get(2 + i).unwrap().as_str();
			if !value.is_empty() {
				let value = value.parse().unwrap();
				res.insert(n + i as u32, value);
			}
		}
	}

	res
}

pub fn parse_bins(text: &str) -> HashMap<u32, Bin> {
	let mut res = HashMap::new();

	for caps in BIN_REGEX.captures_iter(text) {
		let n = caps.get(1).unwrap().as_str();
		let snr = caps.get(2).unwrap().as_str();
		let gain = caps.get(3).unwrap().as_str();
		let bits = caps.get(4).unwrap().as_str();

		res.insert(
			n.parse().unwrap(),
			Bin {
				snr: snr.parse().unwrap(),
				gain: gain.parse().unwrap(),
				bits: bits.parse().unwrap(),
			},
		);
	}

	res
}

pub async fn write_bins_csv(bins: &HashMap<u32, Bin>, output: &mut (impl AsyncWrite + Unpin)) -> anyhow::Result<()> {
	let mut bins = bins.iter().map(|(&n, b)| (n, b)).collect::<Vec<_>>();
	bins.sort_by_key(|(n, _)| *n);

	output.write_all(b"bin,snr,gain,bits\n").await?;

	let mut temp = String::with_capacity(64);
	for (n, value) in bins {
		temp.clear();
		writeln!(temp, "{},{},{},{}", n, value.snr, value.gain, value.bits)?;
		output.write_all(temp.as_bytes()).await?;
	}

	Ok(())
}

pub async fn write_delt_csv(bins: &HashMap<u32, i32>, output: &mut (impl AsyncWrite + Unpin)) -> anyhow::Result<()> {
	let mut bins = bins.iter().map(|(&n, b)| (n, b)).collect::<Vec<_>>();
	bins.sort_by_key(|(n, _)| *n);

	output.write_all(b"bin,value\n").await?;

	let mut temp = String::with_capacity(64);
	for (n, value) in bins {
		temp.clear();
		writeln!(temp, "{},{}", n, value)?;
		output.write_all(temp.as_bytes()).await?;
	}

	Ok(())
}

use std::num::ParseFloatError;

use anyhow::anyhow;
use prometheus_exporter::prometheus::{
	register_gauge, register_gauge_vec, register_int_gauge, register_int_gauge_vec, Gauge, GaugeVec, IntGauge, IntGaugeVec,
};
use regex::Regex;

fn parse_float_quirk(s: &str) -> Result<f64, ParseFloatError> {
	if let Ok(v) = s.parse() {
		return Ok(v);
	}

	let neg = s.contains('-');
	let s = s.replace(['-', ' '], "");

	let v: f64 = s.parse()?;
	if neg {
		Ok(-v)
	} else {
		Ok(v)
	}
}

pub enum DataType {
	Int,
	Float,
}

enum GenericGaugeVec {
	Int(IntGaugeVec),
	Float(GaugeVec),
}

enum GenericGauge {
	Int(IntGauge),
	Float(Gauge),
}

pub struct RegexMetric {
	name: &'static str,
	gauge: GenericGauge,
	regex: Regex,
}

impl RegexMetric {
	pub fn new(name: &'static str, description: &str, regex: &str, data_type: DataType) -> RegexMetric {
		RegexMetric {
			name,
			gauge: match data_type {
				DataType::Int => GenericGauge::Int(
					register_int_gauge!(&format!("modem_{name}"), description).expect("Failed to register metric"),
				),
				DataType::Float => GenericGauge::Float(
					register_gauge!(&format!("modem_{name}"), description).expect("Failed to register metric"),
				),
			},
			regex: Regex::new(regex).expect("Failed to construct regex"),
		}
	}

	pub fn update(&self, text: &str) -> anyhow::Result<bool> {
		match self.regex.captures(text) {
			Some(caps) => {
				let cap = caps
					.get(1)
					.ok_or_else(|| anyhow!("Regular expression capture group 1 did not match"))?;
				let value_str = cap.as_str();

				match &self.gauge {
					GenericGauge::Int(m) => {
						let value = value_str.parse()?;
						m.set(value);

						tracing::trace!(name = self.name, value, "Metric updated");
					}
					GenericGauge::Float(m) => {
						let value = parse_float_quirk(value_str)?;
						m.set(value);

						tracing::trace!(name = self.name, value, "Metric updated");
					}
				}

				Ok(true)
			}
			None => Ok(false),
		}
	}
}

pub struct DoubleRegexMetric {
	name: String,
	gauge: GenericGaugeVec,
	regex: Regex,
	group1_label: &'static str,
	group2_label: &'static str,
}

impl DoubleRegexMetric {
	pub fn new(
		name: &str,
		description: &str,
		regex: &str,
		data_type: DataType,
		group1_label: &'static str,
		group2_label: &'static str,
	) -> DoubleRegexMetric {
		let metric_name = format!("modem_{name}");

		DoubleRegexMetric {
			name: name.to_owned(),
			gauge: match data_type {
				DataType::Int => GenericGaugeVec::Int(
					register_int_gauge_vec!(&metric_name, description, &["which"]).expect("Failed to register metric"),
				),
				DataType::Float => GenericGaugeVec::Float(
					register_gauge_vec!(&metric_name, description, &["which"]).expect("Failed to register metric"),
				),
			},
			regex: Regex::new(regex).expect("Failed to construct regex"),
			group1_label,
			group2_label,
		}
	}

	pub fn new_ne_fe(name: &str, description: &str, regex: &str, data_type: DataType) -> DoubleRegexMetric {
		Self::new(name, description, regex, data_type, "near", "far")
	}

	pub fn new_ds_us(name: &str, description: &str, regex: &str, data_type: DataType) -> DoubleRegexMetric {
		Self::new(name, description, regex, data_type, "downstream", "upstream")
	}

	pub fn new_ne_fe_more(name: &'static str) -> DoubleRegexMetric {
		let re = format!(r"{name}\s*:\s*(\d+)\s+(\d+)");
		let metric_name = format!("more_{}", name.to_lowercase());
		Self::new_ne_fe(&metric_name, name, &re, DataType::Int)
	}

	pub fn update(&self, text: &str) -> anyhow::Result<bool> {
		match self.regex.captures(text) {
			Some(caps) => {
				let cap1 = caps
					.get(1)
					.ok_or_else(|| anyhow!("Regular expression capture group 1 did not match"))?;
				let cap2 = caps
					.get(2)
					.ok_or_else(|| anyhow!("Regular expression capture group 2 did not match"))?;

				let str1 = cap1.as_str();
				let str2 = cap2.as_str();

				match &self.gauge {
					GenericGaugeVec::Int(m) => {
						let v1 = str1.parse()?;
						let v2 = str2.parse()?;
						m.with_label_values(&[self.group1_label]).set(v1);
						m.with_label_values(&[self.group2_label]).set(v2);

						tracing::trace!(name = self.name, v1, v2, "Metric updated");
					}
					GenericGaugeVec::Float(m) => {
						let v1 = parse_float_quirk(str1)?;
						let v2 = parse_float_quirk(str2)?;
						m.with_label_values(&[self.group1_label]).set(v1);
						m.with_label_values(&[self.group2_label]).set(v2);

						tracing::trace!(name = self.name, v1, v2, "Metric updated");
					}
				}

				Ok(true)
			}
			None => Ok(false),
		}
	}
}

pub enum Metric {
	Regex(RegexMetric),
	DoubleRegex(DoubleRegexMetric),
}

impl Metric {
	pub fn update(&self, text: &str) -> anyhow::Result<bool> {
		match self {
			Metric::Regex(m) => m.update(text),
			Metric::DoubleRegex(m) => m.update(text),
		}
	}

	pub fn name(&self) -> &str {
		match self {
			Metric::Regex(m) => m.name,
			Metric::DoubleRegex(m) => &m.name,
		}
	}
}

impl From<RegexMetric> for Metric {
	fn from(value: RegexMetric) -> Self {
		Metric::Regex(value)
	}
}

impl From<DoubleRegexMetric> for Metric {
	fn from(value: DoubleRegexMetric) -> Self {
		Metric::DoubleRegex(value)
	}
}

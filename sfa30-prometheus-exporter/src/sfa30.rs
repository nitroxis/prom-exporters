#![allow(dead_code)]

use std::{thread, time::Duration};

use crc_all::CrcAlgo;
use embedded_hal::i2c::I2c;

#[derive(Debug, Clone, Copy)]
pub enum Command {
	StartContinuousMeasurement = 0x0006,
	StopContinuousMeasurement = 0x0104,
	ReadMeasuredValues = 0x0327,
	ReadDeviceMarking = 0xD060,
	Reset = 0xD304,
}

const ADDRESS: u8 = 0x5D;

pub struct Sfa30<T> {
	comm: T,
	address: u8,
}

#[derive(Debug)]
pub struct Measurement {
	pub formaldehyde: f32,
	pub humidity: f32,
	pub temperature: f32,
}

const CRC: CrcAlgo<u8> = CrcAlgo::<u8>::new(0x31, 8, 0xff, 0, false);

fn calculate_crc(data: &[u8]) -> u8 {
	let mut crc = 0;
	CRC.init_crc(&mut crc);
	CRC.update_crc(&mut crc, data);
	CRC.finish_crc(&crc)
}

impl<T, E> Sfa30<T>
where
	T: I2c<Error = E>,
{
	pub fn new(i2c: T) -> Self {
		Sfa30 {
			comm: i2c,
			address: ADDRESS,
		}
	}

	pub fn new_with_address(i2c: T, address: u8) -> Self {
		Sfa30 { comm: i2c, address }
	}

	fn write(&mut self, command: Command) -> Result<(), E> {
		let buf = &(command as u16).to_be_bytes();
		tracing::trace!(?command, data = ?&buf, "write");
		self.comm.write(self.address, buf)
	}

	fn write_read(&mut self, command: Command, res: &mut [u8]) -> Result<(), E> {
		let buf = &(command as u16).to_be_bytes();
		tracing::trace!(?command, data = ?buf, "write");
		self.comm.write(self.address, &(command as u16).to_be_bytes())?;
		thread::sleep(Duration::from_millis(5));
		self.comm.read(self.address, res)?;
		tracing::trace!(?res, "read");
		Ok(())
	}

	pub fn soft_reset(&mut self) -> Result<(), E> {
		self.write(Command::Reset)
	}

	pub fn start_measuring(&mut self) -> Result<(), E> {
		self.write(Command::StartContinuousMeasurement)
	}

	pub fn stop_measuring(&mut self) -> Result<(), E> {
		self.write(Command::StopContinuousMeasurement)
	}

	pub fn read_device_marking(&mut self) -> Result<String, E> {
		let mut buf = [0u8; 48];

		self.write_read(Command::ReadDeviceMarking, &mut buf)?;

		let mut str = String::with_capacity(32);
		for chunk in buf.chunks_exact(3) {
			if chunk[2] != calculate_crc(&chunk[0..2]) {
				panic!("CRC mismatch");
			}

			if chunk[0] == 0 {
				break;
			}
			str.push(chunk[0] as char);

			if chunk[1] == 0 {
				break;
			}
			str.push(chunk[1] as char);
		}

		Ok(str)
	}

	pub fn read(&mut self) -> Result<Measurement, E> {
		let mut buf = [0u8; 3 * 3];

		self.write_read(Command::ReadMeasuredValues, &mut buf)?;

		if buf[2] != calculate_crc(&buf[0..2]) || buf[5] != calculate_crc(&buf[3..5]) || buf[8] != calculate_crc(&buf[6..8]) {
			panic!("CRC mismatch");
		}

		Ok(Measurement {
			formaldehyde: i16::from_be_bytes([buf[0], buf[1]]) as f32 / 5.0f32,
			humidity: i16::from_be_bytes([buf[3], buf[4]]) as f32 / 100.0f32,
			temperature: i16::from_be_bytes([buf[6], buf[7]]) as f32 / 200.0f32,
		})
	}
}

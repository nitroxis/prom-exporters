use std::{
	sync::{
		atomic::{AtomicBool, Ordering},
		Arc,
	},
	thread,
	time::Duration,
};

use clap::Parser;
use linux_embedded_hal::I2cdev;
use prometheus_exporter::prometheus::register_gauge;
use sfa30::Sfa30;
use signal_hook::consts::{SIGINT, SIGTERM};
use tracing_subscriber::{fmt, prelude::*, EnvFilter};

mod sfa30;

#[derive(Debug, Parser)]
struct Opts {
	/// The HTTP listen address
	#[clap(short, long, default_value = "0.0.0.0:9188")]
	listen_addr: String,

	/// The I²C address.
	#[clap(short, long, default_value = "93")]
	addr: u8,

	/// Issue a stop-measuring command before starting measurement.
	#[clap(long)]
	stop: bool,

	/// Issue a soft-reset before starting measurement.
	#[clap(long)]
	reset: bool,

	/// The measurement interval (in seconds).
	#[clap(long, default_value = "3")]
	interval: f32,

	/// The I²C device path (e.g. /dev/i2c-0).
	device: String,
}

fn main() {
	tracing_subscriber::registry()
		.with(fmt::layer())
		.with(EnvFilter::from_default_env())
		.init();

	let opts = Opts::parse();

	prometheus_exporter::start(opts.listen_addr.parse().expect("parse listen addr"))
		.expect("failed to start prometheus exporter");

	let metric_formaldehyde =
		register_gauge!("sfa30_formaldehyde", "sfa30 formaldehyde measurement in ppb").expect("register metric");
	let metric_humidity = register_gauge!("sfa30_humidity", "sfa30 humidity in percent").expect("register metric");
	let metric_temperature = register_gauge!("sfa30_temperature", "sfa30 temperature in celsius").expect("register metric");

	let i2c_bus = I2cdev::new(&opts.device).expect("open i2c");
	let mut sfa30 = Sfa30::new_with_address(i2c_bus, opts.addr);

	if opts.stop {
		let res = sfa30.stop_measuring();
		tracing::info!(?res, "Stop measuring");
		thread::sleep(Duration::from_secs(1));
	}

	if opts.reset {
		tracing::info!("Soft reset");
		sfa30.soft_reset().expect("soft reset");
		thread::sleep(Duration::from_secs(1));
	}

	let device_marking = sfa30.read_device_marking().expect("read device marking");
	tracing::info!(device_marking, "Device marking");

	let exit = Arc::new(AtomicBool::new(false));

	signal_hook::flag::register(SIGTERM, exit.clone()).expect("SIGTERM handler");
	signal_hook::flag::register(SIGINT, exit.clone()).expect("SIGINT handler");

	tracing::info!("Starting measuring");
	sfa30.start_measuring().expect("start measuring");
	thread::sleep(Duration::from_secs(1));

	while !exit.load(Ordering::Relaxed) {
		let res = sfa30.read().expect("read");
		tracing::debug!(?res, "Measurement");

		metric_formaldehyde.set(res.formaldehyde as f64);
		metric_humidity.set(res.humidity as f64);
		metric_temperature.set(res.temperature as f64);
		thread::sleep(Duration::from_secs_f32(opts.interval));
	}

	tracing::info!("Stopping measuring");
	sfa30.stop_measuring().expect("stop measuring");
}

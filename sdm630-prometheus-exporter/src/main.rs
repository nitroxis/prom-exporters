use std::time::Duration;

use clap::Parser;
use prometheus_exporter::prometheus::register_gauge;
use tokio::time::sleep;
use tokio_modbus::prelude::*;
use tokio_serial::SerialStream;
use tracing_subscriber::{fmt, prelude::*, EnvFilter};

const PARAMETER_NUMBERS: &[u8] = &[1, 2, 3, 4, 5, 6, 7, 8, 9, 27, 37, 38];

#[derive(Debug, Parser)]
struct Opts {
	/// The HTTP listen address
	#[clap(short, long, default_value = "0.0.0.0:9194")]
	listen_addr: String,

	/// The TTY device path (e.g. /dev/ttyUSB0).
	device: String,

	/// The slave address.
	#[clap(short, long, default_value = "1")]
	slave: u8,
}

#[tokio::main(flavor = "current_thread")]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
	tracing_subscriber::registry()
		.with(fmt::layer())
		.with(EnvFilter::from_default_env())
		.init();

	let opts = Opts::parse();

	let metrics = PARAMETER_NUMBERS
		.iter()
		.map(|n| {
			register_gauge!(format!("sdm630_parameter_{n}"), format!("Eastron SDM630 parameter #{n}"))
				.expect("failed to register gauge")
		})
		.collect::<Vec<_>>();

	prometheus_exporter::start(opts.listen_addr.parse().expect("parse listen addr")).expect("can not start exporter");

	tracing::info!("Opening serial device...");
	let slave = Slave(opts.slave);
	let builder = tokio_serial::new(opts.device, 9600);
	let port = SerialStream::open(&builder).unwrap();

	tracing::info!("Connecting...");
	let mut ctx = rtu::attach_slave(port, slave);

	tracing::info!("Connected");

	loop {
		for (i, param_no) in PARAMETER_NUMBERS.iter().cloned().enumerate() {
			let reg = (param_no as u16 - 1) * 2;
			let rsp = ctx.read_input_registers(reg, 2).await?;
			let bytes = [
				(rsp[0] >> 8) as u8,
				(rsp[0] & 0xFF) as u8,
				(rsp[1] >> 8) as u8,
				(rsp[1] & 0xFF) as u8,
			];

			let value = f32::from_be_bytes(bytes);
			tracing::debug!(param_no, value, "parameter update");
			metrics[i].set(value as f64);
		}

		sleep(Duration::from_millis(5000)).await;
	}
}

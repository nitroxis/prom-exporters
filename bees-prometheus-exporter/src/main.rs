use std::{collections::HashMap, fs, path::PathBuf, time::Duration};

use clap::Parser;
use prometheus_exporter::prometheus::{core::MetricVec, register_int_gauge_vec};
use regex::Regex;
use serde::Deserialize;
use tracing_subscriber::{fmt, prelude::*, EnvFilter};

fn default_interval() -> f64 {
	10.0
}

#[derive(Debug, Deserialize)]
struct DiskConfig {
	stats_file: PathBuf,
}

#[derive(Debug, Deserialize)]
struct Config {
	#[serde(default = "default_interval")]
	interval: f64,

	#[serde(flatten)]
	disks: HashMap<String, DiskConfig>,
}

#[derive(Debug, Parser)]
struct Opts {
	/// The HTTP listen address
	#[clap(short, long, default_value = "0.0.0.0:9189")]
	listen_addr: String,

	/// Path to the TOML configuration file
	config: PathBuf,
}

fn main() {
	tracing_subscriber::registry()
		.with(fmt::layer())
		.with(EnvFilter::from_default_env())
		.init();

	let opts = Opts::parse();

	prometheus_exporter::start(opts.listen_addr.parse().expect("parse listen addr"))
		.expect("failed to start prometheus exporter");

	let config = fs::read_to_string(opts.config).expect("Failed to read config file");
	let config: Config = toml::from_str(&config).expect("Failed to parse config file");

	tracing::info!(?config, "Config");

	let interval = Duration::from_secs_f64(config.interval);
	let pattern = Regex::new(r"(\w+)\=([\w\.]+)").unwrap();

	let mut metrics: HashMap<String, MetricVec<_>> = HashMap::new();

	loop {
		for (name, config) in config.disks.iter() {
			match std::fs::read_to_string(&config.stats_file) {
				Ok(stats) => {
					let start = stats.find("TOTAL:");
					let end = stats.find("RATES:");

					match (start, end) {
						(Some(start), Some(end)) => {
							let stats = &stats[(start + 6)..end];

							for stat in pattern.captures_iter(stats) {
								let key = stat.get(1).expect("Failed to match metric key").as_str();
								let value = stat.get(2).expect("Failed to match metric value").as_str();
								let value: i64 = value.parse().expect("Failed to parse metric value");

								let metric = match metrics.get(key) {
									Some(m) => m.clone(),
									None => {
										let gauge_name = format!("bees_{key}");
										let gauge_desc = format!("Bees statistic '{key}'");
										let gauge = register_int_gauge_vec!(&gauge_name, &gauge_desc, &["disk"])
											.expect("Failed to register gauge");

										metrics.insert(key.to_owned(), gauge.clone());
										gauge
									}
								};

								tracing::debug!(?key, ?value, "Update metric");
								metric.with_label_values(&[name]).set(value);
							}
						}
						_ => {
							tracing::warn!("Failed to parse stats file");
						}
					}
				}
				Err(err) => tracing::warn!(%err, "Failed to read stats file"),
			}
		}

		std::thread::sleep(interval);
	}
}

use anyhow::*;
use chrono::{DateTime, Local, TimeZone};
use clap::Parser;
use pretty_hex::HexConfig;
use std::{
	collections::HashSet,
	io::prelude::*,
	net::{TcpListener, TcpStream},
	thread,
	time::Duration,
};
use tracing::*;
use tracing_subscriber::{fmt, prelude::*, EnvFilter};

/// Prometheus exporter for the Growatt Wi-Fi Data Logger (ShineWiFi-X).
#[derive(Parser, Debug)]
#[clap(author, version, about, long_about = None)]
struct Args {
	/// Hostname and port for the prometheus HTTP server.
	#[clap(short, long, default_value = "0.0.0.0:9192")]
	listen_addr: String,

	/// Hostname and port to listen on for dataloggers.
	#[clap(short, long, default_value = "0.0.0.0:5279")]
	datalogger_listen_addr: String,
}

/// Metrics exported by prometheus_exporter.
mod metrics {
	use prometheus_exporter::prometheus::*;
	const LABELS: &[&str] = &["serial"];
	const LABELS_MULTI: &[&str] = &["serial", "index"];

	lazy_static::lazy_static! {
		pub static ref PV_STATUS: IntGaugeVec = register_int_gauge_vec!("growatt_pv_status", "PV status", LABELS).unwrap();
		pub static ref PV_POWER_IN: GaugeVec = register_gauge_vec!("growatt_pv_power_in", "PV input power in watts", LABELS).unwrap();
		pub static ref PV_VOLTAGE: GaugeVec = register_gauge_vec!("growatt_pv_voltage", "PV voltage in volts", LABELS_MULTI).unwrap();
		pub static ref PV_CURRENT: GaugeVec = register_gauge_vec!("growatt_pv_current", "PV current in ampere", LABELS_MULTI).unwrap();
		pub static ref PV_WATT: GaugeVec = register_gauge_vec!("growatt_pv_watt", "PV power in watt", LABELS_MULTI).unwrap();
		pub static ref PV_POWER_OUT: GaugeVec = register_gauge_vec!("growatt_pv_power_out", "PV output power in watts", LABELS).unwrap();
		pub static ref GRID_FREQUENCY: GaugeVec = register_gauge_vec!("growatt_grid_frequency", "Grid frequency in Hz", LABELS).unwrap();
		pub static ref GRID_VOLTAGE: GaugeVec = register_gauge_vec!("growatt_grid_voltage", "Grid voltage in volts", LABELS_MULTI).unwrap();
		pub static ref GRID_CURRENT: GaugeVec = register_gauge_vec!("growatt_grid_current", "Grid current in ampere", LABELS_MULTI).unwrap();
		pub static ref GRID_WATT: GaugeVec = register_gauge_vec!("growatt_grid_watt", "Grid power in watts", LABELS_MULTI).unwrap();
		pub static ref WORK_TIME: GaugeVec = register_gauge_vec!("growatt_work_time", "Total work time of device", LABELS).unwrap();
		pub static ref ENERGY_TODAY: GaugeVec = register_gauge_vec!("growatt_energy_today", "Energy today", LABELS).unwrap();
		pub static ref ENERGY_TOTAL: GaugeVec = register_gauge_vec!("growatt_energy_total", "Energy total", LABELS).unwrap();
		pub static ref PV_TEMPERATURE: GaugeVec = register_gauge_vec!("growatt_pv_temperature", "PV temperature", LABELS).unwrap();
		pub static ref PV_IPM_TEMPERATURE: GaugeVec = register_gauge_vec!("growatt_pv_ipm_temperature", "IPM temperature", LABELS).unwrap();
		pub static ref P_BUS_VOLTAGE: GaugeVec = register_gauge_vec!("growatt_p_bus_voltage", "P bus voltage", LABELS).unwrap();
		pub static ref N_BUS_VOLTAGE: GaugeVec = register_gauge_vec!("growatt_n_bus_voltage", "N bus voltage", LABELS).unwrap();
	}
}

/// Common voltage + current + watt readout.
struct PowerStat {
	pub voltage: f64,
	pub current: f64,
	pub watt: f64,
}

fn main() {
	tracing_subscriber::registry()
		.with(fmt::layer())
		.with(EnvFilter::from_default_env())
		.init();

	let args = Args::parse();

	prometheus_exporter::start(args.listen_addr.parse().unwrap()).expect("Failed to start prometheus exporter");

	let listener = TcpListener::bind(args.datalogger_listen_addr).expect("Failed to bind TCP listener");

	for stream in listener.incoming() {
		let stream = stream.expect("Failed to accept TCP client");

		stream
			.set_read_timeout(Some(Duration::from_secs(5 * 60)))
			.expect("Failed to set read timeout");

		stream
			.set_write_timeout(Some(Duration::from_secs(5 * 60)))
			.expect("Failed to set write timeout");

		thread::spawn(|| {
			let addr = stream.peer_addr().ok().map_or("?".to_owned(), |a| a.to_string());

			let span = info_span!("Peer", %addr);
			let _enter = span.enter();

			info!("New connection");

			if let Err(e) = handle_connection(stream) {
				error!(?e, "Connection handler error");
			}

			info!("Connection closed");
		});
	}
}

fn handle_connection(mut stream: TcpStream) -> anyhow::Result<()> {
	let mut buffer = [0; 4096];

	let serials: HashSet<String> = HashSet::new();

	let mut serials = scopeguard::guard(serials, |serials| {
		// Reset values when connection is lost.
		for serial in serials.iter() {
			info!(%serial, "Resetting metrics");
			let _ = metrics::PV_STATUS.remove_label_values(&[serial]);
			let _ = metrics::PV_POWER_IN.remove_label_values(&[serial]);
			for index in 0..2 {
				let index = index.to_string();
				let _ = metrics::PV_VOLTAGE.remove_label_values(&[serial, &index]);
				let _ = metrics::PV_CURRENT.remove_label_values(&[serial, &index]);
				let _ = metrics::PV_WATT.remove_label_values(&[serial, &index]);
			}
			let _ = metrics::PV_POWER_OUT.remove_label_values(&[serial]);
			let _ = metrics::GRID_FREQUENCY.remove_label_values(&[serial]);
			for index in 0..3 {
				let index = index.to_string();
				let _ = metrics::GRID_VOLTAGE.remove_label_values(&[serial, &index]);
				let _ = metrics::GRID_CURRENT.remove_label_values(&[serial, &index]);
				let _ = metrics::GRID_WATT.remove_label_values(&[serial, &index]);
			}
			let _ = metrics::WORK_TIME.remove_label_values(&[serial]);
			let _ = metrics::ENERGY_TODAY.remove_label_values(&[serial]);
			let _ = metrics::ENERGY_TOTAL.remove_label_values(&[serial]);
			let _ = metrics::PV_TEMPERATURE.remove_label_values(&[serial]);
			let _ = metrics::PV_IPM_TEMPERATURE.remove_label_values(&[serial]);
			let _ = metrics::P_BUS_VOLTAGE.remove_label_values(&[serial]);
			let _ = metrics::N_BUS_VOLTAGE.remove_label_values(&[serial]);
		}
	});

	loop {
		// Read first 6 bytes.
		// 2 bytes sequence number(?) + 2 bytes protocol version(?) + 2 bytes payload length.
		stream.read_exact(&mut buffer[..6]).context("Failed to read message header")?;

		let protocol = u16::from_be_bytes(buffer[2..4].try_into().unwrap());
		let length = u16::from_be_bytes(buffer[4..6].try_into().unwrap()) as usize;

		if protocol != 6 {
			// TODO: support more protocols?
			bail!("Unsupported protocol {protocol}");
		}

		// 4KiB ought to be enough for anyone.
		if !(2..=4088).contains(&length) {
			bail!("Invalid message length: {length}");
		}

		// 6 bytes header + n bytes payload + 2 bytes for checksum
		let buffer = &mut buffer[..6 + length + 2];

		// Read rest of message.
		stream
			.read_exact(&mut buffer[6..])
			.context("Failed to read message payload")?;

		// Compare checksums.
		let crc = u16::from_be_bytes(buffer[6 + length..6 + length + 2].try_into().unwrap());
		let crc_computed = crc16(&buffer[..length + 6]);

		if crc != crc_computed {
			bail!("Checksum does not match");
		}

		// Cut checksum.
		let buffer = &mut buffer[..6 + length];
		let command = u16::from_be_bytes(buffer[6..8].try_into().unwrap());

		// "Decrypt" or whatever this is supposed to be.
		apply_xor(&mut buffer[8..]);

		match command {
			// Ping
			0x0116 => {
				send_message(&mut stream, buffer).context("Failed to send message")?;
			}
			// Announcement.
			0x0103 => {
				send_ack(&mut stream, buffer).context("Failed to send acknowledgement")?;
			}
			// This is the interesting message containing stats:
			0x0104 | 0x0150 => {
				send_ack(&mut stream, buffer).context("Failed to send acknowledgement")?;

				// Read serial and remember it so that the stats can be reset when this connection is closed.
				let serial = String::from_utf8_lossy(&buffer[0x26..0x30]);
				serials.insert(serial.to_string());

				let date = read_date(buffer, 0x44)?;

				let pv_status = read_u16(buffer, 0x4f)?;
				let pv_power_in = read_u16f(buffer, 0x51, 0.01)?;
				let pv_power = &[read_power(buffer, 0x55)?, read_power(buffer, 0x5d)?];
				let pv_power_out = read_u32f(buffer, 0x7d, 0.1)?;

				let grid_freq = read_u16f(buffer, 0x81, 0.01)?;
				let grid_power = &[
					read_power(buffer, 0x83)?,
					read_power(buffer, 0x8b)?,
					read_power(buffer, 0x93)?,
				];

				let work_time_seconds = read_u32f(buffer, 0xad, 0.5)?;

				let pv_energy_today = read_u32f(buffer, 0xb1, 0.1)?;
				let pv_energy_total = read_u32f(buffer, 0xb5, 0.1)?;

				let pv_temp = read_u16f(buffer, 0x109, 0.1)?;
				let pv_ipm_temp = read_u16f(buffer, 0x111, 0.1)?;

				let p_bus_voltage = read_u16f(buffer, 0x113, 0.1)?;
				let n_bus_voltage = read_u16f(buffer, 0x115, 0.1)?;

				// Only update metrics for current data (0x0104).
				if command == 0x0104 {
					info!(date = %date.to_rfc3339(), "Updating metrics");

					metrics::PV_STATUS.with_label_values(&[&serial]).set(pv_status as i64);
					metrics::PV_POWER_IN.with_label_values(&[&serial]).set(pv_power_in);

					for (index, stat) in pv_power.iter().enumerate() {
						let index = index.to_string();
						metrics::PV_VOLTAGE.with_label_values(&[&serial, &index]).set(stat.voltage);
						metrics::PV_CURRENT.with_label_values(&[&serial, &index]).set(stat.current);
						metrics::PV_WATT.with_label_values(&[&serial, &index]).set(stat.watt);
					}

					metrics::PV_POWER_OUT.with_label_values(&[&serial]).set(pv_power_out);

					metrics::GRID_FREQUENCY.with_label_values(&[&serial]).set(grid_freq);

					for (index, stat) in grid_power.iter().enumerate() {
						let index = index.to_string();
						metrics::GRID_VOLTAGE.with_label_values(&[&serial, &index]).set(stat.voltage);
						metrics::GRID_CURRENT.with_label_values(&[&serial, &index]).set(stat.current);
						metrics::GRID_WATT.with_label_values(&[&serial, &index]).set(stat.watt);
					}

					metrics::WORK_TIME.with_label_values(&[&serial]).set(work_time_seconds);

					metrics::ENERGY_TODAY.with_label_values(&[&serial]).set(pv_energy_today);
					metrics::ENERGY_TOTAL.with_label_values(&[&serial]).set(pv_energy_total);

					metrics::PV_TEMPERATURE.with_label_values(&[&serial]).set(pv_temp);
					metrics::PV_IPM_TEMPERATURE.with_label_values(&[&serial]).set(pv_ipm_temp);

					metrics::P_BUS_VOLTAGE.with_label_values(&[&serial]).set(p_bus_voltage);
					metrics::N_BUS_VOLTAGE.with_label_values(&[&serial]).set(n_bus_voltage);
				}
			}
			0x0119 => {
				let register = read_u16(buffer, 0x26)?;
				let length = read_u16(buffer, 0x28)? as usize;
				let val_bytes = &buffer[0x2a..0x2a + length];
				let value = String::from_utf8_lossy(val_bytes);

				debug!(register, %value, "Config");
			}
			_ => {
				warn!(command, "Unknown command");

				let cfg = HexConfig {
					title: false,
					width: 16,
					group: 0,
					..HexConfig::default()
				};

				println!("{}", pretty_hex::config_hex(&buffer, cfg));
			}
		}
	}
}

fn send_message(stream: &mut TcpStream, buffer: &[u8]) -> Result<()> {
	let mut copy = buffer.to_vec();

	apply_xor(&mut copy[8..]);

	let checksum2 = crc16(&copy);

	copy.push((checksum2 >> 8) as u8);
	copy.push((checksum2 & 0xFF) as u8);

	stream.write_all(&copy).context("Failed to write message")?;

	Ok(())
}

fn send_ack(stream: &mut TcpStream, buffer: &[u8]) -> Result<()> {
	let mut response = [0u8; 9];
	response.copy_from_slice(&buffer[..9]);

	// Set length to 3
	response[4] = 0;
	response[5] = 3;

	// Set data to a single 0 byte.
	response[8] = 0;

	// Send.
	send_message(stream, &response).context("Failed to send message")?;

	Ok(())
}

fn read_date(buffer: &[u8], offset: usize) -> Result<DateTime<Local>> {
	if offset + 6 >= buffer.len() {
		bail!("Failed to read date");
	}

	let year = 2000 + buffer[offset] as i32;
	let month = buffer[offset + 1] as u32;
	let day = buffer[offset + 2] as u32;
	let hour = buffer[offset + 3] as u32;
	let minute = buffer[offset + 4] as u32;
	let second = buffer[offset + 5] as u32;
	chrono::Local
		.with_ymd_and_hms(year, month, day, hour, minute, second)
		.latest()
		.ok_or_else(|| anyhow!("Invalid date"))
}

fn read_u16(buf: &[u8], offset: usize) -> Result<u16> {
	Ok(u16::from_be_bytes(
		buf[offset..offset + 2].try_into().context("Failed to read u16")?,
	))
}

fn read_u32(buf: &[u8], offset: usize) -> Result<u32> {
	Ok(u32::from_be_bytes(
		buf[offset..offset + 4].try_into().context("Failed to read u32")?,
	))
}

fn read_u16f(buf: &[u8], offset: usize, mul: f64) -> Result<f64> {
	Ok(read_u16(buf, offset)? as f64 * mul)
}

fn read_u32f(buf: &[u8], offset: usize, mul: f64) -> Result<f64> {
	Ok(read_u32(buf, offset)? as f64 * mul)
}

fn read_power(buf: &[u8], pos: usize) -> Result<PowerStat> {
	Ok(PowerStat {
		voltage: read_u16f(buf, pos, 0.1)?,
		current: read_u16f(buf, pos + 2, 0.1)?,
		watt: read_u32f(buf, pos + 4, 0.1)?,
	})
}

const XOR_MASK: [u8; 7] = [0x47, 0x72, 0x6F, 0x77, 0x61, 0x74, 0x74];
fn apply_xor(data: &mut [u8]) {
	for i in 0..data.len() {
		data[i] ^= XOR_MASK[i % XOR_MASK.len()];
	}
}

fn crc16(buf: &[u8]) -> u16 {
	let mut crc = 0xFFFFu16;

	for b in buf {
		crc ^= *b as u16;

		for _ in 0..8 {
			if (crc & 0x0001) != 0 {
				crc >>= 1;
				crc ^= 0xA001;
			} else {
				crc >>= 1;
			}
		}
	}

	crc
}

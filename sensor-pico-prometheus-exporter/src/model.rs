use serde::Deserialize;

#[derive(Debug, Deserialize)]
pub struct Scd30Measurement {
	pub co2: f32,
	pub humidity: f32,
	pub temperature: f32,
}

#[derive(Debug, Deserialize)]
pub struct Sfa30Measurement {
	pub formaldehyde: f32,
	pub humidity: f32,
	pub temperature: f32,
}

#[derive(Debug, Deserialize)]
pub struct Bme280Measurement {
	pub temperature: f32,
	pub pressure: f32,
	pub humidity: f32,
}

#[derive(Debug, Deserialize)]
pub struct Sgp40Measurement {
	pub voc: u16,
}

#[derive(Debug, Deserialize)]
pub struct Sps30Measurement {
	pub mass_pm1_0: f32,
	pub mass_pm2_5: f32,
	pub mass_pm4_0: f32,
	pub mass_pm10: f32,
	pub number_pm0_5: f32,
	pub number_pm1_0: f32,
	pub number_pm2_5: f32,
	pub number_pm4_0: f32,
	pub number_pm10: f32,
	pub typical_size: f32,
}

#[derive(Debug, Deserialize)]
pub struct GmxxxMeasurement {
	pub gm102b: f32,
	pub gm302b: f32,
	pub gm502b: f32,
	pub gm702b: f32,
}

#[derive(Debug, Deserialize)]
pub struct SensorMeasurement {
	pub bme280: Bme280Measurement,
	pub sgp40: Sgp40Measurement,
	pub scd30: Scd30Measurement,
	pub sps30: Sps30Measurement,
	pub sfa30: Sfa30Measurement,
	pub gmxxx: GmxxxMeasurement,
}

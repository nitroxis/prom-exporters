use std::{
	io::{BufRead, BufReader},
	sync::{
		atomic::{AtomicBool, Ordering},
		Arc,
	},
	thread,
	time::Duration,
};

use clap::Parser;
use prometheus_exporter::prometheus::{register_gauge, register_gauge_vec};
use serialport::SerialPortType;
use tracing_subscriber::{fmt, prelude::*, EnvFilter};

use crate::model::SensorMeasurement;

mod model;

#[derive(Debug, Parser)]
struct Opts {
	/// The HTTP listen address
	#[clap(short, long, default_value = "0.0.0.0:9187")]
	listen_addr: String,

	/// The TTY device path (e.g. /dev/ttyACM0).
	#[clap(short, long)]
	device: Option<String>,

	/// The baud rate
	#[clap(long, default_value = "115200")]
	baud_rate: u32,
}

fn find_port() -> String {
	let ports = serialport::available_ports().expect("No ports found!");
	for p in ports {
		if let SerialPortType::UsbPort(usb) = &p.port_type {
			if usb.vid == 0x1337 && usb.pid == 0x4242 {
				return p.port_name;
			}
		}
	}

	panic!("No port found");
}

fn main() {
	tracing_subscriber::registry()
		.with(fmt::layer())
		.with(EnvFilter::from_default_env())
		.init();

	let opts = Opts::parse();

	prometheus_exporter::start(opts.listen_addr.parse().expect("parse listen addr"))
		.expect("failed to start prometheus exporter");

	let metric_bme280_temperature =
		register_gauge!("bme280_temperature", "BME280 temperature in celsius").expect("register metric");
	let metric_bme280_humidity = register_gauge!("bme280_humidity", "BME280 relative humidity in %").expect("register metric");
	let metric_bme280_pressure = register_gauge!("bme280_pressure", "BME280 pressure in pascal").expect("register metric");

	let metric_sgp40_voc = register_gauge!("sgp40_voc_index", "SGP40 VOC index").expect("register metric");

	let metric_scd30_co2 = register_gauge!("scd30_co2", "SCD30 CO2 measurement in ppm").expect("register metric");
	let metric_scd30_humidity = register_gauge!("scd30_humidity", "SCD30 humidity in percent").expect("register metric");
	let metric_scd30_temperature =
		register_gauge!("scd30_temperature", "SCD30 temperature in celsius").expect("register metric");

	let metric_sfa30_formaldehyde =
		register_gauge!("sfa30_formaldehyde", "sfa30 formaldehyde measurement in ppb").expect("register metric");
	let metric_sfa30_humidity = register_gauge!("sfa30_humidity", "sfa30 humidity in percent").expect("register metric");
	let metric_sfa30_temperature =
		register_gauge!("sfa30_temperature", "sfa30 temperature in celsius").expect("register metric");

	let metric_sps30_mc =
		register_gauge_vec!("sps30_mc", "SPS30 mass concentration [µg / m³]", &["pm"]).expect("register metric");
	let metric_sps30_nc =
		register_gauge_vec!("sps30_nc", "SPS30 Number Concentration [# / cm³]", &["pm"]).expect("register metric");
	let metric_sps30_typical_particle_size =
		register_gauge!("sps30_typical_particle_size", "SPS30 typical particle size [µm]").expect("register metric");
	let metric_sps30_mc_1p0 = metric_sps30_mc.with_label_values(&["1.0"]);
	let metric_sps30_mc_2p5 = metric_sps30_mc.with_label_values(&["2.5"]);
	let metric_sps30_mc_4p0 = metric_sps30_mc.with_label_values(&["4.0"]);
	let metric_sps30_mc_10p0 = metric_sps30_mc.with_label_values(&["10.0"]);
	let metric_sps30_nc_0p5 = metric_sps30_nc.with_label_values(&["0.5"]);
	let metric_sps30_nc_1p0 = metric_sps30_nc.with_label_values(&["1.0"]);
	let metric_sps30_nc_2p5 = metric_sps30_nc.with_label_values(&["2.5"]);
	let metric_sps30_nc_4p0 = metric_sps30_nc.with_label_values(&["4.0"]);
	let metric_sps30_nc_10p0 = metric_sps30_nc.with_label_values(&["10.0"]);

	let metric_gm102b = register_gauge!("gm102b", "GM102B readout").expect("register metric");
	let metric_gm302b = register_gauge!("gm302b", "GM302B readout").expect("register metric");
	let metric_gm502b = register_gauge!("gm502b", "GM502B readout").expect("register metric");
	let metric_gm702b = register_gauge!("gm702b", "GM702B readout").expect("register metric");

	let mut port = match opts.device {
		Some(path) => serialport::new(path, opts.baud_rate),
		None => serialport::new(find_port(), opts.baud_rate),
	}
	.timeout(Duration::from_millis(10000))
	.open()
	.expect("Failed to open port");

	let running = Arc::new(AtomicBool::new(true));
	let r = running.clone();

	ctrlc::set_handler(move || {
		r.store(false, Ordering::SeqCst);
	})
	.expect("Error setting Ctrl-C handler");

	while running.load(Ordering::SeqCst) {
		tracing::debug!("Writing...");
		port.write_all(b"\n").unwrap();

		tracing::debug!("Reading...");
		let mut reader = BufReader::new(&mut port);
		let mut line = String::with_capacity(1024);
		reader.read_line(&mut line).unwrap();

		let data: SensorMeasurement = serde_json::from_str(&line).unwrap();
		tracing::debug!(?data, "Read data");

		metric_bme280_temperature.set(data.bme280.temperature as _);
		metric_bme280_humidity.set(data.bme280.humidity as _);
		metric_bme280_pressure.set(data.bme280.pressure as _);
		metric_sgp40_voc.set(data.sgp40.voc as _);
		metric_scd30_co2.set(data.scd30.co2 as _);
		metric_scd30_humidity.set(data.scd30.humidity as _);
		metric_scd30_temperature.set(data.scd30.temperature as _);
		metric_sfa30_formaldehyde.set(data.sfa30.formaldehyde as _);
		metric_sfa30_humidity.set(data.sfa30.humidity as _);
		metric_sfa30_temperature.set(data.sfa30.temperature as _);
		metric_sps30_typical_particle_size.set(data.sps30.typical_size as _);
		metric_sps30_mc_1p0.set(data.sps30.mass_pm1_0 as _);
		metric_sps30_mc_2p5.set(data.sps30.mass_pm2_5 as _);
		metric_sps30_mc_4p0.set(data.sps30.mass_pm4_0 as _);
		metric_sps30_mc_10p0.set(data.sps30.mass_pm10 as _);
		metric_sps30_nc_0p5.set(data.sps30.number_pm0_5 as _);
		metric_sps30_nc_1p0.set(data.sps30.number_pm1_0 as _);
		metric_sps30_nc_2p5.set(data.sps30.number_pm2_5 as _);
		metric_sps30_nc_4p0.set(data.sps30.number_pm4_0 as _);
		metric_sps30_nc_10p0.set(data.sps30.number_pm10 as _);
		metric_gm102b.set(data.gmxxx.gm102b as _);
		metric_gm302b.set(data.gmxxx.gm302b as _);
		metric_gm502b.set(data.gmxxx.gm502b as _);
		metric_gm702b.set(data.gmxxx.gm702b as _);

		thread::sleep(Duration::from_millis(5000));
	}
}
